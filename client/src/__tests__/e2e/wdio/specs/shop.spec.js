import HomePage from '../pages/home.page';
import ShopPage from '../pages/shop.page';
import { relativeUrls } from '../../common/relative-urls';
import { getProductsMap, getTargetProductsCount } from '../../common/data-handlers';
import { getCollectionProducts, getRandomCollection, getRandomProduct } from '../../common/data-providers';
import { clearLocalStorage } from '../../../utils/page-interactors';
import {browser} from "@wdio/globals";

describe('Shop', () => {
  const homePage = new HomePage();
  const shopPage = new ShopPage();
  const productsToAddCount = 2;
  const randomCollection = getRandomCollection();
  const previewProducts = getCollectionProducts(randomCollection, true);
  const previewTargetProducts = getProductsMap(previewProducts, productsToAddCount);

  beforeEach(async () => {
    await homePage.open(relativeUrls.shop);
  });

  afterEach(async () => {
    await clearLocalStorage();
  });

  it('cart should have proper state if no products have been added', async () => {
    await expect(homePage.cartItemsCounter).toHaveText('0');
    await homePage.openCart();
    await expect(homePage.getEmptyCartMessage()).toBeDisplayed();
    await expect(homePage.getCheckoutButton()).toBeDisplayed();
  });

  it('user should be able to add products to the cart from "Preview" page', async () => {
    const targetProductsCount = getTargetProductsCount(previewTargetProducts);

    await shopPage.addProductsToCart(previewTargetProducts);
    await expect(shopPage.cartItemsCounter).toHaveText(targetProductsCount.toString());
  });

  it('user should be able to add products to the cart from "Overview" page', async () => {
    const overviewProducts = getCollectionProducts(randomCollection);
    const targetProducts = getProductsMap(overviewProducts, productsToAddCount);
    const targetProductsCount = getTargetProductsCount(targetProducts);

    await shopPage.open(relativeUrls.collection(randomCollection.routeName));
    await shopPage.addProductsToCart(targetProducts);
    await expect(shopPage.cartItemsCounter).toHaveText(targetProductsCount.toString());
  });

  it('user should be able to add products to the cart from "Search Results" page', async () => {
    const product = getRandomProduct();
    const targetProducts = getProductsMap([ product ], 1);
    const targetProductsCount = getTargetProductsCount(targetProducts);

    await shopPage.searchForProduct(product.name);
    await shopPage.addProductToCart(targetProducts[0]);
    await expect(shopPage.cartItemsCounter).toHaveText(targetProductsCount.toString());
  });

  it('added product properties should be displayed in cart properly', async () => {
    const targetProductsCount = getTargetProductsCount(previewTargetProducts);

    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.openCart();
    await expect(shopPage.cartItemsCounter).toHaveText(targetProductsCount.toString());
    previewTargetProducts.forEach(async (product) => {
      const {
        id, name, count, price,
      } = product;

      await expect(shopPage.getCartItemByProductId(id)).toBeDisplayed();
      await expect(shopPage.getCartItemByProductId(id)).toContain(name);
      await expect(shopPage.getCartItemByProductId(id)).toBeDisplayed(`${count}x${price}`);
    });
  });
});
