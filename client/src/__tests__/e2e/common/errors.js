const errors = {
  alreadyRegisteredEmail: 'The email address is already in use by another account.',
  emptyRequiredField: 'Please fill out this field.',
  invalidEmail: 'Please include an \'@\' in the email address.',
  tooShortPassword: 'Password should be at least 6 characters',
  passwordDoesNotMatch: "Password doesn't match",
  invalidPassword: 'The password is invalid or the user does not have a password.',
  nonRegisteredUser: 'There is no user record corresponding to this identifier. The user may have been deleted.',
  chargeError: 'Charge Error: Please check used payment card',
};

export default errors;
