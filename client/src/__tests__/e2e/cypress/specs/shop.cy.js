import ShopPage from '../pages/shop.page';
import HomePage from '../pages/homepage.page';
import { relativeUrls } from '../../common/relative-urls';
import { getProductsMap, getTargetProductsCount } from '../../common/data-handlers';
import { getCollectionProducts, getRandomCollection, getRandomProduct } from '../../common/data-providers';

describe('Shop', () => {
  const homePage = new HomePage();
  const shopPage = new ShopPage();
  const productsToAddCount = 2;
  const randomCollection = getRandomCollection();
  const previewProducts = getCollectionProducts(randomCollection, true);
  const previewTargetProducts = getProductsMap(previewProducts, productsToAddCount);

  beforeEach(() => {
    homePage.open(relativeUrls.home);
  });

  it('cart should have proper state if no products have been added', () => {
    homePage.cartItemsCounter.should('contain', '0');
    shopPage.openCart();
    shopPage.emptyCartMessage.should('be.visible');
    shopPage.goToCheckoutButton.should('be.visible');
  });

  it('user should be able to add products to the cart from "Preview" page', () => {
    const targetProductsCount = getTargetProductsCount(previewTargetProducts);

    homePage.open(relativeUrls.shop);
    shopPage.addProductsToCart(previewTargetProducts);
    shopPage.cartItemsCounter.should('contain', targetProductsCount);
  });

  it('user should be able to add products to the cart from "Overview" page', () => {
    const overviewProducts = getCollectionProducts(randomCollection);
    const targetProducts = getProductsMap(overviewProducts, productsToAddCount);
    const targetProductsCount = getTargetProductsCount(targetProducts);

    homePage.open(relativeUrls.collection(randomCollection.routeName));
    shopPage.addProductsToCart(targetProducts);
    shopPage.cartItemsCounter.should('contain', targetProductsCount);
  });

  it('user should be able to add products to the cart from "Search Results" page', () => {
    const product = getRandomProduct();
    const targetProducts = getProductsMap([ product ], 1);
    const targetProductsCount = getTargetProductsCount(targetProducts);

    homePage.searchForProduct(product.name);
    shopPage.addProductsToCart(targetProducts);
    shopPage.cartItemsCounter.should('contain', targetProductsCount);
  });

  it('added product properties should be displayed in cart properly', () => {
    homePage.open(relativeUrls.shop);
    shopPage.addProductsToCart(previewTargetProducts);
    shopPage.openCart();
    shopPage.cartItems.should('have.length', productsToAddCount);
    previewTargetProducts.forEach((product) => {
      const productTotal = `${product.count}x${product.price}`;
      shopPage.getCartItemByProductId(product.id).should('contain', productTotal);
    });
  });
});
