import BasePage from './base.page';

class PaymentsPage extends BasePage {
  constructor(page) {
    super(page);
    this.page = page;
    this.paymentsFrame = page.frameLocator('iframe');
    this.emailField = this.paymentsFrame.locator('#email');
    this.shippingNameField = this.paymentsFrame.locator('#shipping-name');
    this.shippingAddressField = this.paymentsFrame.locator('#shipping-street');
    this.shippingPostalCodeField = this.paymentsFrame.locator('#shipping-zip');
    this.shippingCityField = this.paymentsFrame.locator('#shipping-city');
    this.shippingCountrySelect = this.paymentsFrame.locator('#shipping-country');
    this.cardNumberField = this.paymentsFrame.locator('#card_number');
    this.cardExpirationDateField = this.paymentsFrame.locator('#cc-exp');
    this.cvcField = this.paymentsFrame.locator('#cc-csc');
    this.closeIcon = this.paymentsFrame.locator('a.close');
    this.paymentsTitle = this.paymentsFrame.locator('.title');
    this.sameAddressCheckbox = this.paymentsFrame.locator('.checkbox.same-address');
    this.invalidInputField = this.paymentsFrame.locator('input.invalid');
    this.billingNameField = this.paymentsFrame.locator('#billing-name');
    this.billingAddressField = this.paymentsFrame.locator('#billing-street');
    this.billingPostalCodeField = this.paymentsFrame.locator('#billing-zip');
    this.billingCityField = this.paymentsFrame.locator('#billing-city');
    this.billingCountrySelect = this.paymentsFrame.locator('#billing-country');
    this.backArrow = this.paymentsFrame.locator('a.back');
    this.submitButton = this.paymentsFrame.locator('#submitButton');
    this.billingSwitcher = this.paymentsFrame.locator('a', { hasText: 'Billing' });
  }

  async changeShippingCountry(targetCountry) {
    await this.shippingCountrySelect.selectOption(targetCountry);
  }

  async changeBillingCountry(targetCountry) {
    await this.billingCountrySelect.selectOption(targetCountry);
  }

  async proceedToNextStep() {
    await this.submitButton.click();
  }

  async closePaymentsPopUp() {
    await this.closeIcon.click();
  }

  async backToPersonalDataForm() {
    await this.backArrow.click();
  }

  async openBillingAddressForm() {
    await this.sameAddressCheckbox.click();
    await this.billingSwitcher.click();
  }

  async clearInput(id) {
    await this.paymentsFrame.locator(`#${id}`).clear();
  }

  async enterPersonalData(userData) {
    await this.changeShippingCountry(userData.country);
    if (userData.email) {
      await this.emailField.type(userData.email, { delay: 100 });
    }
    if (userData.name) {
      await this.shippingNameField.type(userData.name, { delay: 100 });
    }
    if (userData.line1) {
      await this.shippingAddressField.type(userData.line1, { delay: 100 });
    }
    if (userData.zip) {
      await this.shippingPostalCodeField.type(userData.zip, { delay: 100 });
    }
    if (userData.city) {
      await this.shippingCityField.clear();
      await this.shippingCityField.type(userData.city, { delay: 100 });
    }
    if (userData.billingAddress) {
      await this.sameAddressCheckbox.click();
      await this.billingSwitcher.click();
      await this.billingCountrySelect.selectOption(userData.billingAddress.country);
      if (userData.billingAddress.name) {
        await this.billingNameField.clear();
        await this.billingNameField.type(userData.billingAddress.name, { delay: 100 });
      } else {
        await this.billingNameField.clear();
      }
      if (userData.billingAddress.line1) {
        await this.billingAddressField.clear();
        await this.billingAddressField.type(userData.billingAddress.line1, { delay: 100 });
      } else {
        await this.billingAddressField.clear();
      }
      if (userData.billingAddress.zip) {
        await this.billingPostalCodeField.clear();
        await this.billingPostalCodeField.type(userData.billingAddress.zip, { delay: 100 });
      } else {
        await this.billingPostalCodeField.clear();
      }
      if (userData.billingAddress.city) {
        await this.billingCityField.clear();
        await this.billingCityField.type(userData.billingAddress.city, { delay: 100 });
      } else {
        await this.billingCityField.clear();
      }
    }
    await this.proceedToNextStep();
  }

  async enterCardData(userData) {
    if (userData.number) {
      await this.cardNumberField.type(userData.number, { delay: 100 });
    }
    if (userData.expDate) {
      await this.cardExpirationDateField.type(userData.expDate, { delay: 100 });
    }
    if (userData.cvv) {
      await this.cvcField.type(userData.cvv, { delay: 100 });
    }
    await this.proceedToNextStep();
  }
}

export default PaymentsPage;
