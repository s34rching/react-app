import { without } from 'lodash';
import { getCollectionProducts, getRandomCollection } from '../../common/data-providers';
import {
  getCartTotal, getProductsMap, getTargetProductsCount, getUpdatedProducts,
} from '../../common/data-handlers';
import HomePage from '../pages/home.page';
import CheckoutPage from '../pages/checkout.page';
import ShopPage from '../pages/shop.page';
import { relativeUrls } from '../../common/relative-urls';
import { clearLocalStorage } from '../../../utils/page-interactors';

describe('Checkout', () => {
  let targetProducts;
  let product;
  let updatedProducts;

  const homePage = new HomePage();
  const shopPage = new ShopPage();
  const checkoutPage = new CheckoutPage();

  const productsToAddCount = 2;
  const randomCollection = getRandomCollection();
  const previewProducts = getCollectionProducts(randomCollection, true);
  const previewTargetProducts = getProductsMap(previewProducts, productsToAddCount);

  beforeEach(async () => {
    await homePage.open(relativeUrls.shop);
  });

  afterEach(async () => {
    await clearLocalStorage();
  });

  it('checkout items should be displayed properly', async () => {
    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.openCart();
    await shopPage.goToCheckout();
    await expect(checkoutPage.checkoutItems).toBeElementsArrayOfSize(productsToAddCount);
  });

  it('added items should be displayed properly', async () => {
    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.open(relativeUrls.checkout);
    previewTargetProducts.forEach(async (targetProduct) => {
      const {
        id, name, count, price,
      } = targetProduct;

      await expect(checkoutPage.getCheckoutItemById(id)).toBeDisplayed();
      await expect(checkoutPage.getCheckoutItemById(id)).toContain(name);
      await expect(checkoutPage.getCheckoutItemById(id)).toContain(count);
      await expect(checkoutPage.getCheckoutItemById(id)).toContain(price);
    });
  });

  it('total should be correct', async () => {
    const totalText = `Total: $${getCartTotal(previewTargetProducts)}`;

    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.openCart();
    await shopPage.goToCheckout();
    await expect(checkoutPage.total).toHaveText(totalText);
  });

  it('new item rows should not be added if user add same products', async () => {
    const cartProductsCount = getTargetProductsCount(previewTargetProducts) * 2;

    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.openCart();
    await shopPage.goToCheckout();
    await expect(checkoutPage.checkoutItems).toBeElementsArrayOfSize(productsToAddCount);
    await checkoutPage.open(relativeUrls.shop);
    await shopPage.addProductsToCart(previewTargetProducts, cartProductsCount);
    await shopPage.openCart();
    await shopPage.goToCheckout();
    await expect(checkoutPage.checkoutItems).toBeElementsArrayOfSize(productsToAddCount);
  });

  it('empty state is shown on the Checkout page when cart is empty', async () => {
    const totalText = 'Total: $0';

    await homePage.openCart();
    await homePage.goToCheckout();
    await expect(checkoutPage.checkoutItems).not.toExist();
    await expect(checkoutPage.total).toHaveText(totalText);
  });

  describe('WHEN a user increases item count', () => {
    beforeEach(async () => {
      targetProducts = getProductsMap(previewProducts, productsToAddCount);
      [ product ] = targetProducts;
      updatedProducts = getUpdatedProducts('increase', targetProducts, product, 1);

      await shopPage.addProductsToCart(targetProducts);
      await shopPage.openCart();
      await shopPage.goToCheckout();
      await checkoutPage.increaseItemCount(product.id, 1);
    });

    it('THEN checkout total should be updated', async () => {
      const updatedTotalText = `Total: $${getCartTotal(updatedProducts)}`;

      await expect(checkoutPage.total).toHaveText(updatedTotalText);
    });

    it('THEN checkout item row count should be updated', async () => {
      const newTargetProductCount = product.count + 1;

      await expect(checkoutPage.getCheckoutItemCounter(product.id))
        .toHaveText(newTargetProductCount.toString());
    });

    it('THEN cart items count should be updated', async () => {
      const newTargetProductsCount = getTargetProductsCount(updatedProducts);

      await expect(checkoutPage.cartItemsCounter).toHaveText(newTargetProductsCount.toString());
      await checkoutPage.openCart();
      updatedProducts.forEach(async (updatedProduct) => {
        const {
          id, name, count, price,
        } = updatedProduct;

        await expect(shopPage.getCartItemByProductId(id)).toBeDisplayed();
        await expect(shopPage.getCartItemByProductId(id)).toContain(name);
        await expect(shopPage.getCartItemByProductId(id)).toBeDisplayed(`${count}x${price}`);
      });
    });
  });

  describe('WHEN a user reduces item count', () => {
    describe('AND changes its count by 1', () => {
      beforeEach(async () => {
        targetProducts = getProductsMap(previewProducts, productsToAddCount);
        [ product ] = targetProducts;
        updatedProducts = getUpdatedProducts('reduce', targetProducts, product, 1);

        await shopPage.addProductsToCart(targetProducts);
        await shopPage.openCart();
        await shopPage.goToCheckout();
        await checkoutPage.reduceItemCount(product.id, 1);
      });

      it('THEN checkout total should be updated', async () => {
        const updatedTotalText = `Total: $${getCartTotal(updatedProducts)}`;

        await expect(checkoutPage.total).toHaveText(updatedTotalText);
      });

      it('THEN checkout item row count should be updated', async () => {
        const newTargetProductCount = product.count - 1;

        await expect(checkoutPage.getCheckoutItemCounter(product.id))
          .toHaveText(newTargetProductCount.toString());
      });

      it('THEN cart items count should be updated', async () => {
        const newTargetProductsCount = getTargetProductsCount(updatedProducts);

        await expect(checkoutPage.cartItemsCounter).toHaveText(newTargetProductsCount.toString());
        await checkoutPage.openCart();
        updatedProducts.forEach(async (updatedProduct) => {
          const {
            id, name, count, price,
          } = updatedProduct;

          await expect(shopPage.getCartItemByProductId(id)).toBeDisplayed();
          await expect(shopPage.getCartItemByProductId(id)).toContain(name);
          await expect(shopPage.getCartItemByProductId(id)).toBeDisplayed(`${count}x${price}`);
        });
      });
    });

    describe('AND removes item completely', () => {
      beforeEach(async () => {
        targetProducts = getProductsMap(previewProducts, productsToAddCount);
        [ product ] = targetProducts;
        updatedProducts = getUpdatedProducts('remove', targetProducts, product);

        await shopPage.addProductsToCart(targetProducts);
        await shopPage.openCart();
        await shopPage.goToCheckout();
        await checkoutPage.reduceItemCount(product.id, product.count);
      });

      it('THEN checkout total should be updated', async () => {
        const updatedTotalText = `Total: $${getCartTotal(updatedProducts)}`;

        await expect(checkoutPage.total).toHaveText(updatedTotalText);
      });

      it('THEN checkout item row count should be updated', async () => {
        const [ remainingProduct ] = without(targetProducts, product);

        await expect(checkoutPage.checkoutItems).toBeElementsArrayOfSize(productsToAddCount - 1);
        await expect(checkoutPage.getCheckoutItemById(remainingProduct.id)).toBeDisplayed();
      });

      it('THEN cart items count should be updated', async () => {
        const newTargetProductsCount = getTargetProductsCount(updatedProducts);

        await expect(checkoutPage.cartItemsCounter).toHaveText(newTargetProductsCount.toString());
        await checkoutPage.openCart();
        updatedProducts.forEach(async (updatedProduct) => {
          const {
            id, name, count, price,
          } = updatedProduct;

          await expect(shopPage.getCartItemByProductId(id)).toBeDisplayed();
          await expect(shopPage.getCartItemByProductId(id)).toContain(name);
          await expect(shopPage.getCartItemByProductId(id)).toBeDisplayed(`${count}x${price}`);
        });
      });
    });
  });

  describe('WHEN a user removes item', () => {
    beforeEach(async () => {
      targetProducts = getProductsMap(previewProducts, productsToAddCount);
      [ product ] = targetProducts;
      updatedProducts = getUpdatedProducts('remove', targetProducts, product);

      await shopPage.addProductsToCart(targetProducts);
      await shopPage.openCart();
      await shopPage.goToCheckout();
      await checkoutPage.removeItem(product.id);
    });

    it('THEN checkout total should be updated', async () => {
      const updatedTotalText = `Total: $${getCartTotal(updatedProducts)}`;

      await expect(checkoutPage.total).toHaveText(updatedTotalText);
    });

    it('THEN checkout item row count should be updated', async () => {
      const [ remainingProduct ] = without(targetProducts, product);

      await expect(checkoutPage.checkoutItems).toBeElementsArrayOfSize(productsToAddCount - 1);
      await expect(checkoutPage.getCheckoutItemById(remainingProduct.id)).toBeDisplayed();
    });

    it('THEN cart items count should be updated', async () => {
      const newTargetProductsCount = getTargetProductsCount(updatedProducts);

      await expect(checkoutPage.cartItemsCounter).toHaveText(newTargetProductsCount.toString());
      await checkoutPage.openCart();
      updatedProducts.forEach(async (updatedProduct) => {
        const {
          id, name, count, price,
        } = updatedProduct;

        await expect(shopPage.getCartItemByProductId(id)).toBeDisplayed();
        await expect(shopPage.getCartItemByProductId(id)).toContain(name);
        await expect(shopPage.getCartItemByProductId(id)).toBeDisplayed(`${count}x${price}`);
      });
    });
  });
});
