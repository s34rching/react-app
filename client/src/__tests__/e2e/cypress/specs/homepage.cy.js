import { forEach } from 'lodash';
import HomePage from '../pages/homepage.page';
import SigningPage from '../pages/signing.page';
import ShopPage from '../pages/shop.page';
import AboutUsPage from '../pages/about-us.page';
import { links, relativeUrls } from '../../common/relative-urls';
import { getAllCollections, getRandomCollection } from '../../common/data-providers';
import { capitalize, toUpper } from '../../common/data-handlers';

describe('Homepage', () => {
  const homePage = new HomePage();
  const signingPage = new SigningPage();
  const shopPage = new ShopPage();
  const aboutUsPage = new AboutUsPage();

  beforeEach(() => {
    homePage.open(relativeUrls.home);
  });

  it('user should be able to open "Shop" overview page', () => {
    const section = getRandomCollection();
    const homepageSectionTitle = toUpper(section.title);
    const shopPageTitle = capitalize(section.title);

    homePage.goToOverviewSection(homepageSectionTitle);
    shopPage.shopContainer.should('be.visible');
    shopPage.shopContainer.should('contain', shopPageTitle);
    shopPage.productCards.should('have.length', section.items.length);
  });

  it('user should be able to send their feedback', () => {
    homePage.openFeedBackForm();
    homePage.feedbackFormTitle.should('be.visible');
  });

  describe('Header', () => {
    it('website logo should lead user to the homepage', () => {
      homePage.open(relativeUrls.signing);
      signingPage.signUpEmail.should('be.visible');
      signingPage.logo.click();
      homePage.directoryMenu.should('be.visible');
    });

    it('user should be able to open "Shop" preview page', () => {
      const titles = getAllCollections().map((collection) => collection.title.toUpperCase());

      homePage.goToShop();
      shopPage.shopContainer.should('be.visible');
      shopPage.previewSectionTitles.should('to.have.members', titles);
    });

    it('user should be able to open "About us" page', () => {
      homePage.goToAboutUs();
      aboutUsPage.getUrl().should('contain', relativeUrls.aboutUs);
    });

    it('user should be able to open "Signing" page', () => {
      homePage.goToSigning();
      signingPage.signUpEmail.should('be.visible');
    });

    it('cart icon should be displayed on home page', () => {
      homePage.cartItemsCounter.should('be.visible');
    });
  });

  describe('Footer', () => {
    forEach(links.internal, (link) => {
      it(`user should be able to navigate to "${link.text}" page`, () => {
        homePage.getFooterLinkByUrl(link.path)
          .should('be.visible')
          .and('have.text', link.text);
      });
    });

    forEach(links.external.images, (link) => {
      it(`user should be able to navigate to company '${link.resource}' page`, () => {
        homePage.getFooterLinkByUrl(link.url)
          .should('be.visible')
          .and('have.attr', 'target', link.target)
          .find('svg')
          .should('have.css', 'width', `${link.width}px`)
          .and('have.css', 'height', `${link.height}px`);
      });
    });

    forEach(links.external.text, (link) => {
      it(`user should be able to navigate to relative external resource: ${link.text}`, () => {
        homePage.getFooterLinkByUrl(link.url)
          .should('be.visible')
          .and('have.text', link.text)
          .and('have.attr', 'target', link.target);
      });
    });
  });
});
