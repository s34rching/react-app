import { times } from 'lodash';
import BasePage from './base.page';

class ShopPage extends BasePage {
  constructor(page) {
    super(page);
    this.page = page;
    this.shopContainer = page.locator('.shop-page');
    this.productsCards = page.locator('[data-test^=item-container-]');
    this.shopSectionTitles = page.locator('.shop-page a[href]');
  }

  async addProductToCart(product) {
    await this.page.locator(`[data-test="item-container-${product.id}"]`).hover();
    times(product.count, async () => {
      await this.page.locator(`[data-test="item-container-${product.id}"] :text-is("Add to Cart")`).click();
    });
    await new Promise((resolve) => setTimeout(resolve, 1000));
  }

  async addProductsToCart(products) {
    await products.reduce(async (previous, next) => {
      await previous;
      await this.addProductToCart(next);
    }, Promise.resolve());
  }
}

export default ShopPage;
