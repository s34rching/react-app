import { expect } from '@playwright/test';

import { times } from 'lodash';
import BasePage from './base.page';

class CheckoutPage extends BasePage {
  constructor(page) {
    super(page);
    this.page = page;
    this.total = page.locator('[data-test="checkout-total"]');
    this.payNowButton = page.locator('span', { hasText: 'Pay Now' });
  }

  async increaseItemCount(productId, count) {
    times(count, async () => {
      await this.page
        .locator(`[data-test="checkout-item-${productId}"]`)
        .locator('[data-test="item-increase-count"]')
        .click();
    });
  }

  async reduceItemCount(productId, count) {
    times(count, async () => {
      await this.page
        .locator(`[data-test="checkout-item-${productId}"]`)
        .locator('[data-test="item-reduce-count"]')
        .click();
    });
  }

  async removeItem(productId) {
    await this.page
      .locator(`[data-test="checkout-item-${productId}"]`)
      .locator('[data-test="item-remove"]')
      .click();
  }

  async openPayments() {
    await this.payNowButton.click();
  }

  async assertCheckoutItems(addedProducts) {
    await Promise.all(
      addedProducts.map(async (product) => {
        const { id, name, count, price } = product;

        await expect(this.page.locator(`[data-test="checkout-item-${id}"]`)).toBeVisible();
        await expect(this.page.locator(`[data-test="checkout-item-${id}"]`)).toContainText(name);
        await expect(this.page.locator(`[data-test="checkout-item-${id}"]`)).toContainText(count.toString());
        await expect(this.page.locator(`[data-test="checkout-item-${id}"]`)).toContainText(price.toString());
      }),
    );
  }

  async assertCheckoutItemCountToBe(number, productId) {
    await expect(this.page.locator(`[data-test="checkout-item-${productId}"]`).locator('[data-test="item-counter"]'))
      .toContainText(number.toString());
  }

  async assertCheckoutItemNotExist(productId) {
    await expect(this.page.locator(`[data-test="checkout-item-${productId}"]`)).toHaveCount(0);
  }
}

export default CheckoutPage;
