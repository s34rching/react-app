import { $, browser } from '@wdio/globals';
import BasePage from './base.page';
import { slowType, waitForElement } from '../../../utils/page-interactors';

class PaymentsPage extends BasePage {
  constructor() {
    super();
    this.paymentsFrameSelector = '[name="stripe_checkout_app"]';
  }

  get paymentsFrame() {
    return $('[name="stripe_checkout_app"]');
  }

  get emailField() {
    return $('#email');
  }

  get shippingNameField() {
    return $('#shipping-name');
  }

  get shippingAddressField() {
    return $('#shipping-street');
  }

  get shippingPostalCodeField() {
    return $('#shipping-zip');
  }

  get shippingCityField() {
    return $('#shipping-city');
  }

  get shippingCountrySelect() {
    return $('#shipping-country');
  }

  get cardNumberField() {
    return $('#card_number');
  }

  get cardExpirationDateField() {
    return $('#cc-exp');
  }

  get cvcField() {
    return $('#cc-csc');
  }

  get closeIcon() {
    return $('a.close');
  }

  get paymentsTitle() {
    return $('.title');
  }

  get sameAddressCheckbox() {
    return $('.checkbox.same-address');
  }

  get invalidInputField() {
    return $('input.invalid');
  }

  get billingNameField() {
    return $('#billing-name');
  }

  get billingAddressField() {
    return $('#billing-street');
  }

  get billingPostalCodeField() {
    return $('#billing-zip');
  }

  get billingCityField() {
    return $('#billing-city');
  }

  get billingCountrySelect() {
    return $('#billing-country');
  }

  get backArrow() {
    return $('a.back');
  }

  get submitButton() {
    return $('#submitButton');
  }

  get billingSwitcher() {
    return $('a=Billing');
  }

  async getPaymentsFrame() {
    await browser.execute(waitForElement, this.paymentsFrameSelector);
    return this.paymentsFrame;
  }

  async changeShippingCountry(targetCountry) {
    await this.shippingCountrySelect.waitForClickable();
    await this.shippingCountrySelect.selectByAttribute('value', targetCountry);
  }

  async changeBillingCountry(targetCountry) {
    await this.billingCountrySelect.waitForClickable();
    await this.billingCountrySelect.selectByAttribute('value', targetCountry);
  }

  async proceedToNextStep() {
    await this.submitButton.click();
  }

  async closePaymentsPopup() {
    await this.closeIcon.waitForClickable();
    await this.closeIcon.click();
  }

  async backToPersonalDataForm() {
    await this.backArrow.waitForClickable();
    await this.backArrow.click();
  }

  async openBillingAddressForm() {
    await this.sameAddressCheckbox.click();
    await this.billingSwitcher.waitForClickable();
    await this.billingSwitcher.click();
  }

  async enterPersonalData(userData) {
    await this.changeShippingCountry(userData.country);
    if (userData.email) {
      await this.emailField.setValue(userData.email);
    }
    if (userData.name) {
      await this.shippingNameField.setValue(userData.name);
    }
    if (userData.line1) {
      await this.shippingAddressField.setValue(userData.line1);
    }
    if (userData.zip) {
      await this.shippingPostalCodeField.setValue(userData.zip);
    }
    if (userData.city) {
      await this.shippingCityField.clearValue();
      await this.shippingCityField.setValue(userData.city);
    }
    if (userData.billingAddress) {
      await this.openBillingAddressForm();
      await this.changeBillingCountry(userData.country);
      if (userData.billingAddress.name) {
        await this.billingNameField.clearValue();
        await this.billingNameField.setValue(userData.billingAddress.name);
      } else {
        await this.billingNameField.clearValue();
      }
      if (userData.billingAddress.line1) {
        await this.billingAddressField.clearValue();
        await this.billingAddressField.setValue(userData.billingAddress.line1);
      } else {
        await this.billingAddressField.clearValue();
      }
      if (userData.billingAddress.zip) {
        await this.billingPostalCodeField.clearValue();
        await this.billingPostalCodeField.setValue(userData.billingAddress.zip);
      } else {
        await this.billingPostalCodeField.clearValue();
      }
      if (userData.billingAddress.city) {
        await this.billingCityField.clearValue();
        await this.billingCityField.setValue(userData.billingAddress.city);
      } else {
        await this.billingCityField.clearValue();
      }
    }
    await this.proceedToNextStep();
  }

  async enterCardData(userData) {
    if (userData.number) {
      for (let i = 0; i < userData.number.length; i++) {
        // eslint-disable-next-line no-await-in-loop
        await slowType(this.cardNumberField, userData.number[i]);
      }
    }
    if (userData.expDate) {
      for (let i = 0; i < userData.expDate.length; i++) {
        // eslint-disable-next-line no-await-in-loop
        await slowType(this.cardExpirationDateField, userData.expDate[i]);
      }
    }
    if (userData.cvv) {
      for (let i = 0; i < userData.cvv.length; i++) {
        // eslint-disable-next-line no-await-in-loop
        await slowType(this.cvcField, userData.cvv[i]);
      }
    }
    await this.proceedToNextStep();
  }

  async getPaymentOperationResultMessage() {
    await browser.waitUntil(async () => browser.isAlertOpen());
    return browser.getAlertText();
  }
}

export default PaymentsPage;
