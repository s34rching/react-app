import BasePage from './base.page';

class AboutUsPage extends BasePage {
  constructor(page) {
    super(page);
    this.page = page;
  }
}

export default AboutUsPage;
