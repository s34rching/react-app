import constants from './constants';

export const relativeUrls = {
  home: '/',
  signing: '/signing',
  search: (query) => `/search=${query}`,
  shop: '/shop',
  collection: (routeName) => `/shop/${routeName}`,
  checkout: '/checkout',
  aboutUs: '/about-us',
  careers: '/careers',
  news: '/news',
  terms: '/terms',
  privacy: '/privacy',
  accessibility: '/accessibility',
  locations: '/locations',
  membership: '/membership',
};

export const links = {
  internal: {
    // careers: { text: 'Careers', path: relativeUrls.careers },
    news: { text: 'News', path: relativeUrls.news },
    terms: { text: 'Terms', path: relativeUrls.terms },
    privacy: { text: 'Privacy', path: relativeUrls.privacy },
    accessibility_policy: { text: 'Accessibility Policy', path: relativeUrls.accessibility },
    locations: { text: 'FIND A STORE', path: relativeUrls.locations },
    sign_up: { text: 'SIGN UP', path: relativeUrls.signing },
    // become_a_member: { text: 'BECOME A MEMBER', path: relativeUrls.membership },
  },
  external: {
    text: {
      react: { text: 'React', url: 'https://reactjs.org/', target: constants.EXTERNAL_LINKS_TARGET },
      redux: { text: 'Redux', url: 'https://redux.js.org/', target: constants.EXTERNAL_LINKS_TARGET },
      express: { text: 'Express', url: 'https://expressjs.com/', target: constants.EXTERNAL_LINKS_TARGET },
      firebase: { text: 'Firebase', url: 'https://firebase.google.com/', target: constants.EXTERNAL_LINKS_TARGET },
      stripe: { text: 'Stripe', url: 'https://stripe.com/', target: constants.EXTERNAL_LINKS_TARGET },
      jest: { text: 'Jest', url: 'https://jestjs.io/', target: constants.EXTERNAL_LINKS_TARGET },
    },
    images: {
      instagram: {
        resource: 'Instagram',
        url: 'https://www.instagram.com/',
        width: constants.SMM_IMAGE_WIDTH,
        height: constants.SMM_IMAGE_HEIGHT,
        target: constants.EXTERNAL_LINKS_TARGET,
      },
      tiktok: {
        resource: 'TikTok',
        url: 'https://www.tiktok.com/en/',
        width: constants.SMM_IMAGE_WIDTH,
        height: constants.SMM_IMAGE_HEIGHT,
        target: constants.EXTERNAL_LINKS_TARGET,
      },
      twitter: {
        resource: 'Twitter',
        url: 'https://www.instagram.com/',
        width: constants.SMM_IMAGE_WIDTH,
        height: constants.SMM_IMAGE_HEIGHT,
        target: constants.EXTERNAL_LINKS_TARGET,
      },
      facebook: {
        resource: 'Facebook',
        url: 'https://www.instagram.com/',
        width: constants.SMM_IMAGE_WIDTH,
        height: constants.SMM_IMAGE_HEIGHT,
        target: constants.EXTERNAL_LINKS_TARGET,
      },
    },
  },
};
