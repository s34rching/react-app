import BasePage from './base.page';

class SigningPage extends BasePage {
  constructor(page) {
    super(page);
    this.page = page;

    this.signInEmail = page.locator("[data-test='sign-in-email']");
    this.signInPassword = page.locator("[data-test='sign-in-password']");
    this.submitSignInButton = page.locator("[data-test='sign-in-submit']");
    this.failedSignInEmailInput = page.locator("[data-test='sign-in-email'][required]:invalid");
    this.failedSignInPasswordInput = page.locator("[data-test='sign-in-password'][required]:invalid");

    this.nameInput = page.locator("[data-test='sign-up-name']");
    this.emailInput = page.locator("[data-test='sign-up-email']");
    this.passwordInput = page.locator("[data-test='sign-up-password']");
    this.confirmPasswordInput = page.locator("[data-test='sign-up-confirm-password']");
    this.submitButton = page.locator("[data-test='sign-up-submit']");
    this.errorMessage = page.locator("[data-test='signing-error']");
    this.failedNameInput = page.locator("[data-test='sign-up-name'][required]:invalid");
    this.failedEmailInput = page.locator("[data-test='sign-up-email'][required]:invalid");
    this.failedPasswordInput = page.locator("[data-test='sign-up-password'][required]:invalid");
    this.failedConfirmPasswordInput = page.locator("[data-test='sign-up-confirm-password'][required]:invalid");
  }

  async signIn({ email, password }) {
    if (email) {
      await this.signInEmail.clear();
      await this.signInEmail.type(email);
    }
    if (password) {
      await this.signInPassword.clear();
      await this.signInPassword.type(password);
    }
    await this.submitSignInButton.click();
  }

  async signUp({ name, email, password, confirmPassword }) {
    if (name) {
      await this.nameInput.clear();
      await this.nameInput.type(name);
    }
    if (email) {
      await this.emailInput.clear();
      await this.emailInput.type(email);
    }
    if (password) {
      await this.passwordInput.clear();
      await this.passwordInput.type(password);
    }
    if (confirmPassword) {
      await this.confirmPasswordInput.clear();
      await this.confirmPasswordInput.type(confirmPassword);
    }
    await this.submitButton.click();
  }
}

export default SigningPage;
