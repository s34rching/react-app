import { test, expect } from '@playwright/test';
import Chance from 'chance';
import SearchResultsPage from '../pages/search-results.page';
import HomePage from '../pages/home.page';
import { relativeUrls } from '../../common/relative-urls';
import { getRandomProduct } from '../../common/data-providers';
import { filterProductsByNameSubstring, getProductNameSubstring } from '../../common/data-handlers';

const chance = new Chance();
const product = getRandomProduct();

test.describe('Search', () => {
  test('user should be able to search for products by exact product name', async ({ page }) => {
    const homePage = new HomePage(page);
    const searchResultsPage = new SearchResultsPage(page);

    await homePage.open(relativeUrls.home);
    await homePage.searchForProduct(product.name);
    await expect(searchResultsPage.foundProductsCards).toBeVisible();
    await expect(searchResultsPage.foundProductsCards).toHaveCount(1);
    await expect(searchResultsPage.foundProductsCards).toHaveAttribute('data-test', `item-container-${product.id}`);
  });

  test('user should be able to search for products by product name substring', async ({ page }) => {
    const homePage = new HomePage(page);
    const searchResultsPage = new SearchResultsPage(page);
    const query = getProductNameSubstring(product.name);
    const expectedProducts = filterProductsByNameSubstring(query);

    await homePage.open(relativeUrls.home);
    await homePage.searchForProduct(query);
    await expect(searchResultsPage.foundProductsCards).toHaveCount(expectedProducts.length);
  });

  test('empty SERP should be displayed when user submits invalid search query', async ({ page }) => {
    const homePage = new HomePage(page);
    const searchResultsPage = new SearchResultsPage(page);
    const query = chance.sentence({ words: 3 });

    await homePage.open(relativeUrls.home);
    await homePage.searchForProduct(query);
    await expect(searchResultsPage.emptyResultsContainer).toBeVisible();
  });

  test('user should not be able to submit empty query', async ({ page, baseURL }) => {
    const homePage = new HomePage(page);
    const query = ' ';

    await homePage.open(relativeUrls.home);
    await homePage.searchForProduct(query);
    await expect(page).not.toHaveURL(`${baseURL}${relativeUrls.search(query)}`);
  });
});
