import { browser, $, $$ } from '@wdio/globals';

class BasePage {
  constructor() {
    this.cartItemsCounterSelector = '[data-test="cart-items-counter"]';
    this.emptyCartMessageSelector = 'span=Your cart is empty';
    this.goToCheckoutButtonSelector = 'button=GO TO CHECKOUT';
  }

  get logo() {
    return $('#site-logo');
  }

  get header() {
    return $('[data-test="header"]');
  }

  get footer() {
    return $('[data-test="footer"]');
  }

  get signOutButton() {
    return $('[data-test="sign-out-button"]');
  }

  get signInButton() {
    return this.header.$('a=SIGN IN');
  }

  get footerSignUpLink() {
    return this.footer.$('a=SIGN UP');
  }

  get searchBar() {
    return $('input[name="search"]');
  }

  get feedbackFormTitle() {
    return $('h2=CUSTOMER FEEDBACK FORM');
  }

  get cartItemsCounter() {
    return $(this.cartItemsCounterSelector);
  }

  async getUrl() {
    return browser.getUrl();
  }

  async getAlertText() {
    return browser.getAlertText();
  }

  async open(url) {
    await browser.url(url);
  }

  async searchForProduct(productName) {
    await this.searchBar.setValue(productName);
    await browser.keys('\uE007');
  }

  async openCart() {
    await this.cartItemsCounter.click();
  }

  async getEmptyCartMessage() {
    await $(this.emptyCartMessageSelector).waitForDisplayed();
    return $(this.emptyCartMessageSelector);
  }

  async getCheckoutButton() {
    await $(this.goToCheckoutButtonSelector).waitForDisplayed();
    return $(this.goToCheckoutButtonSelector);
  }

  async getCartItemByProductId(productId) {
    await $(`[data-test="cart-item-${productId}"]`);
  }

  async getCartItems() {
    return $$('[data-test^="cart-item-"]');
  }

  async goToCheckout() {
    return $(this.goToCheckoutButtonSelector).click();
  }

  async openFeedbackForm() {
    await this.footer.$('h4=SEND US FEEDBACK').click();
  }

  async goToShop() {
    await this.header.$('a=SHOP').click();
  }

  async goToAboutUs() {
    await this.header.$('a=ABOUT US').click();
  }

  async goToSigning() {
    await this.signInButton.click();
  }

  async getFooterLinkByUrl(url) {
    return this.footer.$(`a[href="${url}"]`);
  }

  async getLinkIcon(url) {
    return this.footer.$(`a[href="${url}"] svg`);
  }

  async switchToFrame(frame) {
    await browser.switchToFrame(frame);
  }
}

export default BasePage;
