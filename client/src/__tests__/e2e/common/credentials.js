const credentials = {
  valid: {
    login: 'frameworks.comparison@email.com',
    password: 'the_pa$$w0rd',
  },
  google: {
    login: 'frameworks.comparison@gmail.com',
    password: 'the_pa$$w0rd',
  },
};

export default credentials;
