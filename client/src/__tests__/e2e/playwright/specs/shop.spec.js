import { test, expect } from '@playwright/test';
import HomePage from '../pages/home.page';
import ShopPage from '../pages/shop.page';
import { relativeUrls } from '../../common/relative-urls';
import { getProductsMap, getTargetProductsCount } from '../../common/data-handlers';
import { getCollectionProducts, getRandomCollection, getRandomProduct } from '../../common/data-providers';

test.describe('Shop', () => {
  const productsToAddCount = 2;
  const randomCollection = getRandomCollection();
  const previewProducts = getCollectionProducts(randomCollection, true);
  const previewTargetProducts = getProductsMap(previewProducts, productsToAddCount);

  test('cart should have proper state if no products have been added', async ({ page }) => {
    const homePage = new HomePage(page);

    await homePage.open(relativeUrls.home);
    await expect(homePage.cartItemsCounter).toHaveText('0');
    await homePage.openCart();
    await expect(homePage.cartEmptyMessage).toBeVisible();
    await expect(homePage.goToCheckoutButton).toBeVisible();
  });

  test('user should be able to add products to the cart from "Preview"', async ({ page }) => {
    const targetProductsCount = getTargetProductsCount(previewTargetProducts);

    const homePage = new HomePage(page);
    const shopPage = new ShopPage(page);

    await homePage.open(relativeUrls.shop);
    await shopPage.addProductsToCart(previewTargetProducts);
    await expect(shopPage.cartItemsCounter).toHaveText(targetProductsCount);
  });

  test('user should be able to add products to the cart from "Overview" page', async ({ page }) => {
    const overviewProducts = getCollectionProducts(randomCollection);
    const targetProducts = getProductsMap(overviewProducts, productsToAddCount);
    const targetProductsCount = getTargetProductsCount(targetProducts);

    const homePage = new HomePage(page);
    const shopPage = new ShopPage(page);

    await homePage.open(relativeUrls.collection(randomCollection.routeName));
    await shopPage.addProductsToCart(targetProducts);
    await expect(shopPage.cartItemsCounter).toHaveText(targetProductsCount);
  });

  test('user should be able to add products to the cart from "Search Results" page', async ({ page }) => {
    const product = getRandomProduct();
    const targetProducts = getProductsMap([ product ], 1);
    const targetProductsCount = getTargetProductsCount(targetProducts);

    const homePage = new HomePage(page);
    const shopPage = new ShopPage(page);

    await homePage.open(relativeUrls.home);
    await homePage.searchForProduct(product.name);
    await shopPage.addProductsToCart(targetProducts);
    await expect(shopPage.cartItemsCounter).toHaveText(targetProductsCount);
  });

  test('added product properties should be displayed in cart properly', async ({ page }) => {
    const homePage = new HomePage(page);
    const shopPage = new ShopPage(page);

    await homePage.open(relativeUrls.shop);
    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.openCart();
    await shopPage.assertCartItems(previewTargetProducts);
  });
});
