class BasePage {
  get logo() {
    return cy.get('#site-logo');
  }

  get signOutButton() {
    return cy.get('[data-test="sign-out-button"]');
  }

  get signInButton() {
    return cy.get('[href="/signing"]').contains('SIGN IN');
  }

  get header() {
    return cy.get('[data-test="header"]');
  }

  get footer() {
    return cy.get('[data-test="footer"]');
  }

  get footerSignUpLink() {
    return this.footer.contains('SIGN UP');
  }

  get searchBar() {
    return cy.get('input[name="search"]');
  }

  get cartItemsCounter() {
    return cy.get('[data-test="cart-items-counter"]');
  }

  get cartItems() {
    return cy.get('[data-test^=cart-item-]');
  }

  get emptyCartMessage() {
    return cy.get('span').contains('Your cart is empty');
  }

  get goToCheckoutButton() {
    return cy.get('button').contains('GO TO CHECKOUT');
  }

  get feedbackFormTitle() {
    return cy.contains('CUSTOMER FEEDBACK FORM');
  }

  open(url) {
    cy.visit(url);
  }

  getUrl() {
    return cy.url();
  }

  searchForProduct(productName) {
    this.searchBar.type(`${productName}{enter}`);
  }

  openCart() {
    this.cartItemsCounter.click();
  }

  goToCheckout() {
    this.openCart();
    this.goToCheckoutButton.click();
  }

  getCartItemByProductId(productId) {
    return cy.get(`[data-test=cart-item-${productId}]`);
  }

  goToShop() {
    return this.header.contains('SHOP').click();
  }

  goToAboutUs() {
    return this.header.contains('ABOUT US').click();
  }

  goToSigning() {
    return this.signInButton.click();
  }

  getFooterLinkByUrl(url) {
    return this.footer.find(`a[href="${url}"]`);
  }

  openFeedBackForm() {
    return this.footer.contains('SEND US FEEDBACK').click();
  }
}

export default BasePage;
