import Chance from 'chance';
import HomePage from '../pages/homepage.page';
import SearchResultsPage from '../pages/search-results.page';
import { relativeUrls } from '../../common/relative-urls';
import { getRandomProduct } from '../../common/data-providers';
import { filterProductsByNameSubstring, getProductNameSubstring } from '../../common/data-handlers';

const chance = new Chance();
const homePage = new HomePage();
const searchResultsPage = new SearchResultsPage();
const product = getRandomProduct();

describe('Search', () => {
  beforeEach(() => {
    homePage.open(relativeUrls.home);
  });

  it('user should be able to search for products by exact product name', () => {
    homePage.searchForProduct(product.name);
    searchResultsPage.getFoundProductById(product.id).should('be.visible');
  });

  it('user should be able to search for products by product name substring', () => {
    const query = getProductNameSubstring(product.name);
    const expectedProducts = filterProductsByNameSubstring(query);

    homePage.searchForProduct(query);
    searchResultsPage.getAllFoundProducts()
      .should('have.length', expectedProducts.length)
      .each((card) => {
        cy.wrap(card).contains(new RegExp(`${query}`, 'gi'));
      });
  });

  it('empty SERP should be displayed when user submits invalid search query', () => {
    const query = chance.sentence({ words: 3 });

    homePage.searchForProduct(query);
    searchResultsPage.emptyResultsContainer.should('be.visible');
  });

  it('user should not be able to submit empty query', () => {
    const query = ' ';

    homePage.searchForProduct(query);
    homePage.getUrl().should('not.contain', relativeUrls.search(query));
  });
});
