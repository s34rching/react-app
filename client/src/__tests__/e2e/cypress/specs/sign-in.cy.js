import SigningPage from '../pages/signing.page';
import User from '../../common/entities/user';
import { relativeUrls } from '../../common/relative-urls';
import credentials from '../../common/credentials';
import { getRandomPassword } from '../../common/data-providers';
import errors from '../../common/errors';

describe('Sign In', () => {
  let userData;
  const { login, password } = credentials.valid;
  const signingPage = new SigningPage();

  beforeEach(() => {
    signingPage.open(relativeUrls.signing);
  });

  it('user should be able to sign in with valid data', () => {
    userData = new User({ email: login, password });

    signingPage.signIn(userData);
    signingPage.signOutButton.should('be.visible').contains('SIGN OUT');
  });

  it.skip('user should be able to sign in with Google', () => {
    signingPage.signInWithGoogle();
    signingPage.signOutButton.should('be.visible').contains('SIGN OUT');
  });

  it('user should NOT be able to sign in with wrong password', () => {
    userData = new User({ email: login, password: getRandomPassword() });

    signingPage.signIn(userData);
    signingPage.errorMessage.should('be.visible').contains(errors.invalidPassword);
  });

  it('user should NOT be able to sign in with non-registered email', () => {
    userData = new User();

    signingPage.signIn(userData);
    signingPage.errorMessage.should('be.visible').contains(errors.nonRegisteredUser);
  });

  it('user should NOT be able to sign in with empty email', () => {
    userData = new User();
    userData.email = null;

    signingPage.signIn(userData);
    signingPage.signInEmail.then((input) => {
      expect(input[0].validationMessage).to.eq(errors.emptyRequiredField);
    });
  });

  it('user should NOT be able to sign in with empty password', () => {
    userData = new User({ email: login });
    userData.password = null;

    signingPage.signIn(userData);
    signingPage.signInPassword.then((input) => {
      expect(input[0].validationMessage).to.eq(errors.emptyRequiredField);
    });
  });

  it('"Sign Up" link in footer should not exist if user is signed in', () => {
    userData = new User({ email: login, password });

    signingPage.signIn(userData);
    signingPage.signOutButton.should('be.visible').contains('SIGN OUT');
    signingPage.footerSignUpLink.should('not.exist');
  });

  it('user should be able to sign out', () => {
    userData = new User({ email: login, password });

    signingPage.signIn(userData);
    signingPage.signOutButton.should('be.visible').contains('SIGN OUT');

    signingPage.signOutButton.click();
    signingPage.signInButton.should('be.visible');
  });
});
