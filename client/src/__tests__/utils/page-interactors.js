import { $, browser } from '@wdio/globals';

export function waitForElement(selector) {
  return new Promise((resolve) => {
    if (document.querySelector(selector)) {
      return resolve(document.querySelector(selector));
    }

    const observer = new MutationObserver(() => {
      if (document.querySelector(selector)) {
        resolve(document.querySelector(selector));
        observer.disconnect();
      }
    });

    observer.observe(document.body, {
      childList: true,
      subtree: true,
    });
  });
}

export const clearLocalStorage = async () => {
  await browser.execute('window.localStorage.clear()');
};

export const slowType = async (element, symbol) => {
  await element.addValue(symbol);
  await browser.pause(100);
};

export const waitForAnimation = async (selector) => {
  await $(selector).waitForDisplayed();
  await browser.waitUntil(async () => {
    const oldX = await $(selector).getLocation('x');
    const oldY = await $(selector).getLocation('y');
    await browser.pause(200);
    const newX = await $(selector).getLocation('x');
    const newY = await $(selector).getLocation('y');
    return oldX === newX && oldY === newY;
  });
};
