import { forEach } from 'lodash';
import { browser } from '@wdio/globals';
import HomePage from '../pages/home.page';
import SigningPage from '../pages/signing.page';
import ShopPage from '../pages/shop.page';
import { links, relativeUrls } from '../../common/relative-urls';
import { getAllCollections, getRandomCollection } from '../../common/data-providers';
import { capitalize, toUpper } from '../../common/data-handlers';

describe('Homepage', () => {
  const homePage = new HomePage();
  const signingPage = new SigningPage();
  const shopPage = new ShopPage();

  beforeEach(async () => {
    await homePage.open(relativeUrls.home);
  });

  it('user should be able to open "Shop" overview page', async () => {
    const section = getRandomCollection();
    const homepageSectionTitle = toUpper(section.title);
    const shopPageTitle = capitalize(section.title);

    await homePage.goToOverviewSection(homepageSectionTitle);
    await expect(shopPage.shopContainer).toBeDisplayed();
    await expect(shopPage.shopContainer).toHaveTextContaining(shopPageTitle);
    await expect(shopPage.productCards).toBeElementsArrayOfSize(section.items.length);
  });

  it('user should be able to send their feedback', async () => {
    await homePage.openFeedbackForm();
    await expect(homePage.feedbackFormTitle).toBeDisplayed();
  });

  describe('Header', () => {
    it('website logo should lead user to the homepage', async () => {
      await homePage.open(relativeUrls.signing);
      await expect(signingPage.signInEmail).toBeDisplayed();
      await signingPage.logo.click();
      await expect(homePage.directoryMenu).toBeDisplayed();
    });

    it('user should be able to open "Shop" preview page', async () => {
      await homePage.goToShop();
      await expect(shopPage.shopContainer).toBeDisplayed();
      await expect(shopPage.sectionTitles).toBeElementsArrayOfSize(getAllCollections().length);
    });

    it('user should be able to open "About us" page', async () => {
      await homePage.goToAboutUs();
      await expect(browser).toHaveUrlContaining(relativeUrls.aboutUs);
    });

    it('user should be able to open "Signing" page', async () => {
      await homePage.goToSigning();
      await expect(signingPage.signInEmail).toBeDisplayed();
    });

    it('cart icon should be displayed on home page', async () => {
      await expect(homePage.cartItemsCounter).toBeDisplayed();
    });
  });

  describe('Footer', () => {
    forEach(links.internal, (link) => {
      it(`user should be able to navigate to "${link.text}" page`, async () => {
        await expect(homePage.getFooterLinkByUrl(link.path)).toBeDisplayed();
        await expect(homePage.getFooterLinkByUrl(link.path)).toHaveText(link.text);
      });
    });

    forEach(links.external.images, (link) => {
      it(`user should be able to navigate to company '${link.resource}' page`, async () => {
        await expect(homePage.getFooterLinkByUrl(link.url)).toBeDisplayed();
        await expect(homePage.getFooterLinkByUrl(link.url)).toHaveAttribute('target', link.target);

        const icon = await homePage.getLinkIcon(link.url);
        await expect(await icon.getSize('width')).toEqual(parseInt(link.width, 10));
        await expect(await icon.getSize('height')).toEqual(parseInt(link.height, 10));
      });
    });

    forEach(links.external.text, (link) => {
      it(`user should be able to navigate to relative external resource: ${link.text}`, async () => {
        await expect(homePage.getFooterLinkByUrl(link.url)).toBeDisplayed();
        await expect(homePage.getFooterLinkByUrl(link.url)).toHaveText(link.text);
        await expect(homePage.getFooterLinkByUrl(link.url)).toHaveAttribute('target', link.target);
      });
    });
  });
});
