import BasePage from './base.page';

class HomePage extends BasePage {
  constructor(page) {
    super(page);
    this.page = page;
    this.directoryMenu = page.locator('#directory-menu');
  }

  async goToOverviewSection(sectionTitle) {
    await this.page
      .locator('#directory-menu')
      .locator('span', { hasText: new RegExp(`^${sectionTitle}$`) })
      .click();
  }
}

export default HomePage;
