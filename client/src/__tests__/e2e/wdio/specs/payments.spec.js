import dayjs from 'dayjs';
import { browser } from '@wdio/globals';
import HomePage from '../pages/home.page';
import ShopPage from '../pages/shop.page';
import CheckoutPage from '../pages/checkout.page';
import PaymentsPage from '../pages/payments.page';
import { relativeUrls } from '../../common/relative-urls';
import { getCollectionProducts, getRandomCollection } from '../../common/data-providers';
import { getCartTotal, getProductsMap } from '../../common/data-handlers';
import Address from '../../common/entities/address';
import BillingAddress from '../../common/entities/billing-address';
import PaymentCard from '../../common/entities/payment-card';
import User from '../../common/entities/user';
import errors from '../../common/errors';
import {clearLocalStorage} from "../../../utils/page-interactors";

describe('Payments', () => {
  let userData;

  const homePage = new HomePage();
  const shopPage = new ShopPage();
  const checkoutPage = new CheckoutPage();
  const paymentsPage = new PaymentsPage();
  const productsToAddCount = 2;
  const randomCollection = getRandomCollection();
  const previewProducts = getCollectionProducts(randomCollection, true);
  const previewTargetProducts = getProductsMap(previewProducts, productsToAddCount);

  beforeEach(async () => {
    userData = {
      ...new User(),
      ...new Address(),
      ...new PaymentCard(),
    };

    await homePage.open(relativeUrls.shop);
    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.open(relativeUrls.checkout);
    await checkoutPage.openPayments();

    const paymentsFrame = await paymentsPage.getPaymentsFrame();
    await checkoutPage.switchToFrame(paymentsFrame);
  });

  afterEach(async () => {
    await browser.switchToParentFrame();
    await clearLocalStorage();
  });

  it('user should be able to pay for purchases', async () => {
    await paymentsPage.enterPersonalData(userData);
    await paymentsPage.enterCardData(userData);
    await expect(await paymentsPage.getPaymentOperationResultMessage()).toEqual(errors.chargeError);
  });

  it('user should be able to pay for purchases if billing address is valid', async () => {
    userData.billingAddress = new BillingAddress();

    await paymentsPage.enterPersonalData(userData);
    await paymentsPage.enterCardData(userData);
    await expect(await paymentsPage.getPaymentOperationResultMessage()).toEqual(errors.chargeError);
  });

  it('user should be able to return to previous screen', async () => {
    await paymentsPage.enterPersonalData(userData);
    await paymentsPage.backToPersonalDataForm();
    await expect(paymentsPage.shippingNameField).toBeDisplayed();
  });

  it('user should be able to close payments modal', async () => {
    await paymentsPage.closePaymentsPopup();
    await expect(checkoutPage.total).toBeDisplayed();
  });

  it('cart total should be equal to payment total', async () => {
    const total = getCartTotal(previewTargetProducts);

    await expect(paymentsPage.paymentsTitle).toHaveTextContaining(total);
    await paymentsPage.closePaymentsPopup();
    await expect(checkoutPage.total).toHaveTextContaining(total);
  });

  it('shipping address should be the same as billing address by default', async () => {
    await expect(paymentsPage.sameAddressCheckbox).toBeDisplayed();
    await expect(paymentsPage.sameAddressCheckbox).toHaveAttributeContaining('class', 'checked');
  });

  it('user should not be able to proceed if entered invalid email', async () => {
    userData.email = 'email.com';

    await paymentsPage.enterPersonalData(userData);
    await expect(paymentsPage.invalidInputField).toBeDisplayed();
    await expect(paymentsPage.invalidInputField).toHaveAttributeContaining('id', 'email');
  });

  it('user should not be able to proceed with missing email', async () => {
    userData.email = null;

    await paymentsPage.enterPersonalData(userData);
    await expect(paymentsPage.invalidInputField).toBeDisplayed();
    await expect(paymentsPage.invalidInputField).toHaveAttributeContaining('id', 'email');
  });

  it('user should not be able to proceed with missing name', async () => {
    userData.name = null;

    await paymentsPage.enterPersonalData(userData);
    await expect(paymentsPage.invalidInputField).toBeDisplayed();
    await expect(paymentsPage.invalidInputField).toHaveAttributeContaining('id', 'shipping-name');
  });

  it('user should not be able to proceed with missing street address', async () => {
    userData.line1 = null;

    await paymentsPage.enterPersonalData(userData);
    await expect(paymentsPage.invalidInputField).toBeDisplayed();
    await expect(paymentsPage.invalidInputField).toHaveAttributeContaining('id', 'shipping-street');
  });

  it('user should not be able to proceed with missing postal code', async () => {
    userData.zip = null;

    await paymentsPage.enterPersonalData(userData);
    await expect(paymentsPage.invalidInputField).toBeDisplayed();
    await expect(paymentsPage.invalidInputField).toHaveAttributeContaining('id', 'shipping-zip');
  });

  it('user city should be defined by default if zip code was entered', async () => {
    userData.city = null;

    await paymentsPage.enterPersonalData(userData);
    await expect(paymentsPage.cardNumberField).toBeDisplayed();
  });

  it('user should not be able to proceed if invalid card number was entered', async () => {
    userData.number = new PaymentCard().number.slice(0, 5);

    await paymentsPage.enterPersonalData(userData);
    await paymentsPage.enterCardData(userData);
    await expect(paymentsPage.invalidInputField).toBeDisplayed();
    await expect(paymentsPage.invalidInputField).toHaveAttributeContaining('id', 'card_number');
  });

  it('user should not be able to proceed if card number is missing', async () => {
    userData.number = null;

    await paymentsPage.enterPersonalData(userData);
    await paymentsPage.enterCardData(userData);
    await expect(paymentsPage.invalidInputField).toBeDisplayed();
    await expect(paymentsPage.invalidInputField).toHaveAttributeContaining('id', 'card_number');
  });

  it('user should not be able to proceed if card is expired', async () => {
    userData.expDate = dayjs().subtract(1, 'month').format('MMYY');

    await paymentsPage.enterPersonalData(userData);
    await paymentsPage.enterCardData(userData);
    await expect(paymentsPage.invalidInputField).toBeDisplayed();
    await expect(paymentsPage.invalidInputField).toHaveAttributeContaining('id', 'cc-exp');
  });

  it('user should not be able to proceed if card expiration date is invalid', async () => {
    userData.expDate = '9999';

    await paymentsPage.enterPersonalData(userData);
    await paymentsPage.enterCardData(userData);
    await expect(paymentsPage.invalidInputField).toBeDisplayed();
    await expect(paymentsPage.invalidInputField).toHaveAttributeContaining('id', 'cc-exp');
  });

  it('user should not be able to proceed if card expiration date is missing', async () => {
    userData.expDate = null;

    await paymentsPage.enterPersonalData(userData);
    await paymentsPage.enterCardData(userData);
    await expect(paymentsPage.invalidInputField).toBeDisplayed();
    await expect(paymentsPage.invalidInputField).toHaveAttributeContaining('id', 'cc-exp');
  });

  it('user should not be able to proceed if cvv is invalid', async () => {
    userData.cvv = '1';

    await paymentsPage.enterPersonalData(userData);
    await paymentsPage.enterCardData(userData);
    await expect(paymentsPage.invalidInputField).toBeDisplayed();
    await expect(paymentsPage.invalidInputField).toHaveAttributeContaining('id', 'cc-csc');
  });

  it('user should not be able to proceed if cvv is missing', async () => {
    userData.cvv = null;

    await paymentsPage.enterPersonalData(userData);
    await paymentsPage.enterCardData(userData);
    await expect(paymentsPage.invalidInputField).toBeDisplayed();
    await expect(paymentsPage.invalidInputField).toHaveAttributeContaining('id', 'cc-csc');
  });

  it('user should not be able to pay for purchases if billing name is missing', async () => {
    userData.billingAddress = new BillingAddress();
    userData.billingAddress.name = null;

    await paymentsPage.enterPersonalData(userData);
    await expect(paymentsPage.invalidInputField).toBeDisplayed();
    await expect(paymentsPage.invalidInputField).toHaveAttributeContaining('id', 'billing-name');
  });

  it('user should not be able to pay for purchases if billing street address is missing', async () => {
    userData.billingAddress = new BillingAddress();
    userData.billingAddress.line1 = null;

    await paymentsPage.enterPersonalData(userData);
    await expect(paymentsPage.invalidInputField).toBeDisplayed();
    await expect(paymentsPage.invalidInputField).toHaveAttributeContaining('id', 'billing-street');
  });

  it('user should not be able to pay for purchases if billing postal code is missing', async () => {
    userData.billingAddress = new BillingAddress();
    userData.billingAddress.zip = null;

    await paymentsPage.enterPersonalData(userData);
    await expect(paymentsPage.invalidInputField).toBeDisplayed();
    await expect(paymentsPage.invalidInputField).toHaveAttributeContaining('id', 'billing-zip');
  });

  it('user should not be able to pay for purchases if billing city is missing', async () => {
    userData.billingAddress = new BillingAddress();
    userData.billingAddress.city = null;

    await paymentsPage.enterPersonalData(userData);
    await expect(paymentsPage.invalidInputField).toBeDisplayed();
    await expect(paymentsPage.invalidInputField).toHaveAttributeContaining('id', 'billing-city');
  });
});
