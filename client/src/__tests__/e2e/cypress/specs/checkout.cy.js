import HomePage from '../pages/homepage.page';
import ShopPage from '../pages/shop.page';
import CheckoutPage from '../pages/checkout.page';
import { relativeUrls } from '../../common/relative-urls';
import {
  getProductsMap, getCartTotal, getUpdatedProducts, getTargetProductsCount,
} from '../../common/data-handlers';
import { getCollectionProducts, getRandomCollection } from '../../common/data-providers';

describe('Checkout', () => {
  let targetProducts;
  let product;
  let updatedProducts;

  const homePage = new HomePage();
  const shopPage = new ShopPage();
  const checkoutPage = new CheckoutPage();
  const productsToAddCount = 2;
  const randomCollection = getRandomCollection();
  const previewProducts = getCollectionProducts(randomCollection, true);
  const previewTargetProducts = getProductsMap(previewProducts, productsToAddCount);

  beforeEach(() => {
    homePage.open(relativeUrls.shop);
  });

  it('checkout items count should be equal to number of added products', () => {
    shopPage.addProductsToCart(previewTargetProducts);
    shopPage.goToCheckout();
    checkoutPage.getUrl().should('contain', relativeUrls.checkout);
    checkoutPage.checkoutItems.should('have.length', productsToAddCount);
  });

  it('added items should be displayed properly', () => {
    shopPage.addProductsToCart(previewTargetProducts);
    shopPage.goToCheckout();
    previewTargetProducts.forEach((targetProduct) => {
      checkoutPage.getCheckoutItem(targetProduct.id)
        .should('be.visible')
        .and('contain', targetProduct.name)
        .and('contain', targetProduct.price)
        .and('contain', targetProduct.count);
    });
  });

  it('total should be correct', () => {
    const totalText = `Total: $${getCartTotal(previewTargetProducts)}`;

    shopPage.addProductsToCart(previewTargetProducts);
    shopPage.goToCheckout();
    checkoutPage.total.should('to.have.text', totalText);
  });

  it('new item rows should not be added if user add same products', () => {
    shopPage.addProductsToCart(previewTargetProducts);
    shopPage.goToCheckout();
    shopPage.open(relativeUrls.shop);
    shopPage.addProductsToCart(previewTargetProducts);
    shopPage.goToCheckout();
    checkoutPage.checkoutItems.should('have.length', productsToAddCount);
  });

  it('empty state is shown on the Checkout page when cart is empty', () => {
    const totalText = 'Total: $0';

    shopPage.cartItemsCounter.should('contain', '0');
    shopPage.goToCheckout();
    checkoutPage.checkoutItems.should('have.length', 0);
    checkoutPage.total.should('to.have.text', totalText);
  });

  describe('WHEN a user increases item count', () => {
    beforeEach(() => {
      targetProducts = getProductsMap(previewProducts, productsToAddCount);
      [ product ] = targetProducts;
      updatedProducts = getUpdatedProducts('increase', targetProducts, product, 1);

      shopPage.addProductsToCart(targetProducts);
      shopPage.goToCheckout();
      checkoutPage.increaseItemCount(product.id, 1);
    });

    it('THEN checkout total should be updated', () => {
      const updatedTotalText = `Total: $${getCartTotal(updatedProducts)}`;

      checkoutPage.total.should('have.text', updatedTotalText);
    });

    it('THEN checkout item row count should be updated', () => {
      const newTargetProductCount = product.count + 1;

      checkoutPage.getCheckoutItemCounter(product.id).should('contain', newTargetProductCount);
    });

    it('THEN cart items count should be updated', () => {
      const newTargetProductsCount = getTargetProductsCount(updatedProducts);

      checkoutPage.cartItemsCounter.should('contain', newTargetProductsCount);
      checkoutPage.openCart();
      updatedProducts.forEach((updateProduct) => {
        const productTotal = `${updateProduct.count}x${updateProduct.price}`;
        shopPage.getCartItemByProductId(updateProduct.id).should('contain', productTotal);
      });
    });
  });

  describe('WHEN a user reduces item count', () => {
    describe('AND changes its count by 1', () => {
      beforeEach(() => {
        targetProducts = getProductsMap(previewProducts, productsToAddCount);
        [ product ] = targetProducts;
        updatedProducts = getUpdatedProducts('reduce', targetProducts, product, 1);

        shopPage.addProductsToCart(targetProducts);
        shopPage.goToCheckout();
        checkoutPage.reduceItemCount(product.id, 1);
      });

      it('THEN checkout total should be updated', () => {
        const updatedTotalText = `Total: $${getCartTotal(updatedProducts)}`;

        checkoutPage.total.should('have.text', updatedTotalText);
      });

      it('THEN checkout item row count should be updated', () => {
        const newTargetProductCount = product.count - 1;

        checkoutPage.getCheckoutItemCounter(product.id).should('contain', newTargetProductCount);
      });

      it('THEN cart items count should be updated', () => {
        const newTargetProductsCount = getTargetProductsCount(updatedProducts);

        checkoutPage.cartItemsCounter.should('contain', newTargetProductsCount);
        checkoutPage.openCart();
        updatedProducts.forEach((updateProduct) => {
          const productTotal = `${updateProduct.count}x${updateProduct.price}`;
          shopPage.getCartItemByProductId(updateProduct.id).should('contain', productTotal);
        });
      });
    });

    describe('AND removes item completely', () => {
      beforeEach(() => {
        targetProducts = getProductsMap(previewProducts, productsToAddCount);
        [ product ] = targetProducts;
        updatedProducts = getUpdatedProducts('remove', targetProducts, product);

        shopPage.addProductsToCart(targetProducts);
        shopPage.goToCheckout();
        checkoutPage.reduceItemCount(product.id, product.count);
      });

      it('THEN item should be removed from checkout page', () => {
        checkoutPage.getCheckoutItem(product.id).should('not.exist');
      });

      it('THEN checkout total should be updated', () => {
        const updatedTotalText = `Total: $${getCartTotal(updatedProducts)}`;

        checkoutPage.total.should('have.text', updatedTotalText);
      });

      it('THEN cart items count should be updated', () => {
        const newTargetProductsCount = getTargetProductsCount(updatedProducts);

        checkoutPage.cartItemsCounter.should('contain', newTargetProductsCount);
        checkoutPage.openCart();
        checkoutPage.getCartItemByProductId(product.id).should('not.exist');
        updatedProducts.forEach((updatedProduct) => {
          const productTotal = `${updatedProduct.count}x${updatedProduct.price}`;
          shopPage.getCartItemByProductId(updatedProduct.id).should('contain', productTotal);
        });
      });
    });
  });

  describe('WHEN a user removes item', () => {
    beforeEach(() => {
      targetProducts = getProductsMap(previewProducts, productsToAddCount);
      [ product ] = targetProducts;
      updatedProducts = getUpdatedProducts('remove', targetProducts, product);

      shopPage.addProductsToCart(targetProducts);
      shopPage.goToCheckout();
      checkoutPage.removeItem(product.id);
    });

    it('THEN item should be removed from checkout page', () => {
      checkoutPage.getCheckoutItem(product.id).should('not.exist');
    });

    it('THEN checkout total should be updated', () => {
      const updatedTotalText = `Total: $${getCartTotal(updatedProducts)}`;

      checkoutPage.total.should('have.text', updatedTotalText);
    });

    it('THEN cart items count should be updated', () => {
      const newTargetProductsCount = getTargetProductsCount(updatedProducts);

      checkoutPage.cartItemsCounter.should('contain', newTargetProductsCount);
      checkoutPage.openCart();
      checkoutPage.getCartItemByProductId(product.id).should('not.exist');
      updatedProducts.forEach((updatedProduct) => {
        const productTotal = `${updatedProduct.count}x${updatedProduct.price}`;
        shopPage.getCartItemByProductId(updatedProduct.id).should('contain', productTotal);
      });
    });
  });
});
