import Address from './address';
import { getRandomName } from '../data-providers';

class BillingAddress extends Address {
  constructor({ name, ...otherProps } = {}) {
    super(otherProps);
    this.name = name || getRandomName();
  }
}

export default BillingAddress;
