import { forEach } from 'lodash';
import { test, expect } from '@playwright/test';
import HomePage from '../pages/home.page';
import SigningPage from '../pages/signing.page';
import ShopPage from '../pages/shop.page';
import { links, relativeUrls } from '../../common/relative-urls';
import { getAllCollections, getRandomCollection } from '../../common/data-providers';
import { capitalize, toUpper } from '../../common/data-handlers';

test.describe('Homepage', () => {
  test('user should be able to open "Shop" overview page', async ({ page }) => {
    const section = getRandomCollection();
    const homepageSectionTitle = toUpper(section.title);
    const shopPageTitle = capitalize(section.title);

    const homePage = new HomePage(page);
    const shopPage = new ShopPage(page);

    await homePage.open(relativeUrls.home);
    await homePage.goToOverviewSection(homepageSectionTitle);
    await expect(shopPage.shopContainer).toBeVisible();
    await expect(shopPage.shopContainer).toContainText(shopPageTitle);
    await expect(shopPage.productsCards).toHaveCount(section.items.length);
  });

  test('user should be able to send their feedback', async ({ page }) => {
    const homePage = new HomePage(page);

    await homePage.open(relativeUrls.home);
    await homePage.openFeedbackForm();
    await expect(homePage.feedbackFormTitle).toBeVisible();
  });

  test.describe('Header', () => {
    test('website logo should lead user to the homepage', async ({ page }) => {
      const homePage = new HomePage(page);
      const signingPage = new SigningPage(page);

      await homePage.open(relativeUrls.signing);
      await expect(signingPage.signInEmail).toBeVisible();
      await homePage.logo.click();
      await expect(homePage.directoryMenu).toBeVisible();
    });

    test('user should be able to open "Shop" preview page', async ({ page }) => {
      const homePage = new HomePage(page);
      const shopPage = new ShopPage(page);

      await homePage.open(relativeUrls.home);
      await homePage.goToShop();
      await expect(shopPage.shopContainer).toBeVisible();
      await expect(shopPage.shopSectionTitles).toHaveCount(getAllCollections().length);
    });

    test('user should be able to open "About us" page', async ({ page }) => {
      const homePage = new HomePage(page);

      await homePage.open(relativeUrls.home);
      await homePage.goToAboutUs();
      await expect(page).toHaveURL(relativeUrls.aboutUs);
    });

    test('user should be able to open "Signing" page', async ({ page }) => {
      const homePage = new HomePage(page);
      const signingPage = new SigningPage(page);

      await homePage.open(relativeUrls.home);
      await homePage.goToSigning();
      await expect(signingPage.signInEmail).toBeVisible();
    });

    test('cart icon should be displayed on home page', async ({ page }) => {
      const homePage = new HomePage(page);

      await homePage.open(relativeUrls.home);
      await expect(homePage.cartItemsCounter).toBeVisible();
    });
  });

  test.describe('Footer', () => {
    forEach(links.internal, (link) => {
      test(`user should be able to navigate to "${link.text}"`, async ({ page }) => {
        const homePage = new HomePage(page);

        await homePage.open(relativeUrls.home);
        await homePage.assertFooterLink(link);
      });
    });

    forEach(links.external.images, (link) => {
      test(`user should be able to navigate to company '${link.resource}' page`, async ({ page }) => {
        const homePage = new HomePage(page);

        await homePage.open(relativeUrls.home);
        await homePage.assertFooterIconLink(link);
      });
    });

    forEach(links.external.text, (link) => {
      test(`user should be able to navigate to relative external resource: ${link.text}`, async ({ page }) => {
        const homePage = new HomePage(page);

        await homePage.open(relativeUrls.home);
        await homePage.assertExternalFooterLink(link);
      });
    });
  });
});
