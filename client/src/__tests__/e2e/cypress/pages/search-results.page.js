import BasePage from './basepage.page';

class SearchResultsPage extends BasePage {
  get emptyResultsContainer() {
    return cy.contains('Nothing found');
  }

  getFoundProductById(id) {
    return cy.get(`[data-test=item-container-${id}]`);
  }

  getAllFoundProducts() {
    return cy.get('[data-test^=item-container-]');
  }
}

export default SearchResultsPage;
