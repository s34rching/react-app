import { expect } from '@wdio/globals';
import { relativeUrls } from '../../common/relative-urls';
import User from '../../common/entities/user';
import SigningPage from '../pages/signing.page';
import errors from '../../common/errors';
import credentials from '../../common/credentials';
import { getRandomPassword } from '../../common/data-providers';

describe('Sign In', () => {
  let userData;
  const { login, password } = credentials.valid;
  const signInPage = new SigningPage();

  beforeEach(async () => {
    userData = new User();

    await signInPage.open(relativeUrls.signing);
  });

  it('user should be able to sign in with valid data', async () => {
    userData = new User({ email: login, password });

    await signInPage.signIn(userData);
    await expect(signInPage.signOutButton).toBeDisplayed();
  });

  it('user should NOT be able to sign in with wrong password', async () => {
    userData = new User({ email: login, password: getRandomPassword() });

    await signInPage.signIn(userData);
    await expect(signInPage.errorMessage).toBeDisplayed();
    await expect(signInPage.errorMessage).toHaveText(errors.invalidPassword);
  });

  it('user should NOT be able to sign in with non-registered email', async () => {
    userData = new User();

    await signInPage.signIn(userData);
    await expect(signInPage.errorMessage).toBeDisplayed();
    await expect(signInPage.errorMessage).toHaveText(errors.nonRegisteredUser);
  });

  it('user should NOT be able to sign in with empty email', async () => {
    userData = new User();
    userData.email = null;

    await signInPage.signIn(userData);
    await expect(signInPage.failedSignInEmailField).toBeDisplayed();
  });

  it('user should NOT be able to sign in with empty password', async () => {
    userData = new User();
    userData.password = null;

    await signInPage.signIn(userData);
    await expect(signInPage.failedSignInPasswordField).toBeDisplayed();
  });

  it('"Sign Up" link in footer should not exist if user is signed in', async () => {
    userData = new User({ email: login, password });

    await signInPage.signIn(userData);
    await expect(signInPage.signOutButton).toBeDisplayed();
    await expect(signInPage.footerSignUpLink).not.toBeDisplayed();
  });

  it('user should be able to sign out', async () => {
    userData = new User({ email: login, password });

    await signInPage.signIn(userData);
    await expect(signInPage.signOutButton).toBeDisplayed();

    await signInPage.signOutButton.click();
    await expect(signInPage.signInButton).toBeDisplayed();
  });
});
