import { $, $$ } from '@wdio/globals';
import { times } from 'lodash';
import BasePage from './base.page';

class CheckoutPage extends BasePage {
  get total() {
    return $('[data-test="checkout-total"]');
  }

  get checkoutItems() {
    return $$('[data-test^="checkout-item-"]');
  }

  get payNowButton() {
    return $('span=Pay Now');
  }

  async getCheckoutItemById(productId) {
    return $(`[data-test="checkout-item-${productId}"]`);
  }

  async getCheckoutItemCounter(productId) {
    const itemSelector = `[data-test="checkout-item-${productId}"]`;
    const itemCounterSelector = '[data-test="item-counter"]';

    return $(itemSelector).$(itemCounterSelector);
  }

  async increaseItemCount(productId, count) {
    const itemSelector = `[data-test="checkout-item-${productId}"]`;
    const increaseArrowSelector = '[data-test="item-increase-count"]';

    times(count, async () => {
      await $(itemSelector).$(increaseArrowSelector).waitForClickable();
      await $(itemSelector).$(increaseArrowSelector).click();
    });
  }

  async reduceItemCount(productId, count) {
    const itemSelector = `[data-test="checkout-item-${productId}"]`;
    const increaseArrowSelector = '[data-test="item-reduce-count"]';

    times(count, async () => {
      await $(itemSelector).$(increaseArrowSelector).waitForClickable();
      await $(itemSelector).$(increaseArrowSelector).click();
    });
  }

  async removeItem(productId) {
    const itemSelector = `[data-test="checkout-item-${productId}"]`;
    const removeItemSelector = '[data-test="item-remove"]';

    await $(itemSelector).$(removeItemSelector).waitForClickable();
    await $(itemSelector).$(removeItemSelector).click();
  }

  async openPayments() {
    await this.payNowButton.waitForClickable();
    await this.payNowButton.click();
  }
}

export default CheckoutPage;
