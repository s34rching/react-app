import _ from 'lodash';
import Chance from 'chance';
import constants from './constants';
import names from './data/names.json';
import lastNames from './data/last-names.json';
import domains from './data/domains.json';
import letters from './data/letters.json';
import numbers from './data/numbers.json';
import symbols from './data/symbols.json';
import getMockedState from '../../utils/mock-state-provider';

const chance = new Chance();

const { PREVIEW_ITEMS_NUMBER } = constants;
const { shop } = getMockedState([ 'shop' ]);

export const getRandomName = () => `${_.sample(names)} ${_.sample(lastNames)}`;
export const getRandomEmail = () => {
  const name = _.lowerCase(_.sample(names).replace(/-/g, '.').split(' ')[0]);
  const timestamp = _.now();
  const domain = _.sample(domains);

  return `${name}${timestamp}@${domain}`;
};
export const getRandomPassword = () => {
  const length = _.sample(_.range(6, 15));
  const characters = [ ...letters, ...numbers, ...symbols ];
  return _.join(_.take(_.shuffle(characters), length), '');
};
export const getRandomCount = (minValue, maxValue) => _.sample(_.range(minValue, maxValue));
export const getCollectionProducts = (collection, isPreview) => (
  (isPreview)
    ? _.take(collection.items, PREVIEW_ITEMS_NUMBER)
    : collection.items
);
export const getAllCollections = () => _.values(shop.collections);
export const getRandomCollection = () => _.sample(getAllCollections());
export const getRandomSection = (directorySections) => _.sample(directorySections);
export const getAllProducts = () => _.flatten(_.map(_.values(shop.collections), 'items'));
export const getRandomProduct = () => _.sample(getAllProducts());
export const getValidRandomPhoneNumber = () => chance.phone({ country: 'us', formatted: false });
