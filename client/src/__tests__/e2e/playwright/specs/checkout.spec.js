import { test, expect } from '@playwright/test';
import HomePage from '../pages/home.page';
import ShopPage from '../pages/shop.page';
import CheckoutPage from '../pages/checkout.page';
import { relativeUrls } from '../../common/relative-urls';
import {
  getProductsMap, getCartTotal, getUpdatedProducts, getTargetProductsCount,
} from '../../common/data-handlers';
import { getCollectionProducts, getRandomCollection } from '../../common/data-providers';

test.describe('Checkout', () => {
  let targetProducts;
  let product;
  let updatedProducts;

  const productsToAddCount = 2;
  const randomCollection = getRandomCollection();
  const previewProducts = getCollectionProducts(randomCollection, true);
  const previewTargetProducts = getProductsMap(previewProducts, productsToAddCount);

  test('checkout items should be displayed properly', async ({ page, baseURL }) => {
    const shopPage = new ShopPage(page);
    const checkoutPage = new CheckoutPage(page);

    await shopPage.open(relativeUrls.shop);
    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.goToCheckout();
    await expect(page).toHaveURL(`${baseURL}${relativeUrls.checkout}`);
    await checkoutPage.assertCheckoutItems(previewTargetProducts);
  });

  test('total should be correct', async ({ page }) => {
    const totalText = `Total: $${getCartTotal(previewTargetProducts)}`;

    const shopPage = new ShopPage(page);
    const checkoutPage = new CheckoutPage(page);

    await shopPage.open(relativeUrls.shop);
    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.open(relativeUrls.checkout);
    await expect(checkoutPage.total).toHaveText(totalText);
  });

  test('new item rows should not be added if user add same products', async ({ page }) => {
    const shopPage = new ShopPage(page);
    const checkoutPage = new CheckoutPage(page);
    const addedProducts = previewTargetProducts.map((addedProduct) => (
      { ...addedProduct, count: addedProduct.count * 2 }));

    await shopPage.open(relativeUrls.shop);
    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.goToCheckout();
    await shopPage.open(relativeUrls.shop);
    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.goToCheckout();
    await checkoutPage.assertCheckoutItems(addedProducts);
  });

  test('empty state is shown on the Checkout page when cart is empty', async ({ page }) => {
    const totalText = 'Total: $0';

    const homePage = new HomePage(page);
    const checkoutPage = new CheckoutPage(page);

    await homePage.open(relativeUrls.home);
    await expect(homePage.cartItemsCounter).toHaveText('0');
    await homePage.open(relativeUrls.checkout);
    await expect(checkoutPage.total).toHaveText(totalText);
  });

  test.describe('WHEN a user increases item count', () => {
    test.beforeEach(() => {
      targetProducts = getProductsMap(previewProducts, productsToAddCount);
      [ product ] = targetProducts;
      updatedProducts = getUpdatedProducts('increase', targetProducts, product, 1);
    });

    test('THEN checkout total should be updated', async ({ page }) => {
      const updatedTotalText = `Total: $${getCartTotal(updatedProducts)}`;

      const homePage = new HomePage(page);
      const shopPage = new ShopPage(page);
      const checkoutPage = new CheckoutPage(page);

      await homePage.open(relativeUrls.shop);
      await shopPage.addProductsToCart(targetProducts);
      await shopPage.open(relativeUrls.checkout);
      await checkoutPage.increaseItemCount(product.id, 1);
      await expect(checkoutPage.total).toHaveText(updatedTotalText);
    });

    test('THEN checkout item row count should be updated', async ({ page }) => {
      const newTargetProductCount = product.count + 1;

      const homePage = new HomePage(page);
      const shopPage = new ShopPage(page);
      const checkoutPage = new CheckoutPage(page);

      await homePage.open(relativeUrls.shop);
      await shopPage.addProductsToCart(targetProducts);
      await shopPage.open(relativeUrls.checkout);
      await checkoutPage.increaseItemCount(product.id, 1);
      await checkoutPage.assertCheckoutItemCountToBe(newTargetProductCount, product.id);
    });

    test('THEN cart items count should be updated', async ({ page }) => {
      const newTargetProductsCount = getTargetProductsCount(updatedProducts);

      const homePage = new HomePage(page);
      const shopPage = new ShopPage(page);
      const checkoutPage = new CheckoutPage(page);

      await homePage.open(relativeUrls.shop);
      await shopPage.addProductsToCart(targetProducts);
      await shopPage.open(relativeUrls.checkout);
      await checkoutPage.increaseItemCount(product.id, 1);
      await expect(checkoutPage.cartItemsCounter).toHaveText(newTargetProductsCount.toString());
      await checkoutPage.openCart();
      await checkoutPage.assertCartItems(updatedProducts);
    });
  });

  test.describe('WHEN a user reduces item count', () => {
    test.describe('AND changes its count by 1', () => {
      test.beforeEach(() => {
        targetProducts = getProductsMap(previewProducts, productsToAddCount);
        [ product ] = targetProducts;
        updatedProducts = getUpdatedProducts('reduce', targetProducts, product, 1);
      });

      test('THEN checkout total should be updated', async ({ page }) => {
        const updatedTotalText = `Total: $${getCartTotal(updatedProducts)}`;

        const homePage = new HomePage(page);
        const shopPage = new ShopPage(page);
        const checkoutPage = new CheckoutPage(page);

        await homePage.open(relativeUrls.shop);
        await shopPage.addProductsToCart(targetProducts);
        await shopPage.open(relativeUrls.checkout);
        await checkoutPage.reduceItemCount(product.id, 1);
        await expect(checkoutPage.total).toHaveText(updatedTotalText);
      });

      test('THEN checkout item row count should be updated', async ({ page }) => {
        const newTargetProductCount = product.count - 1;

        const homePage = new HomePage(page);
        const shopPage = new ShopPage(page);
        const checkoutPage = new CheckoutPage(page);

        await homePage.open(relativeUrls.shop);
        await shopPage.addProductsToCart(targetProducts);
        await shopPage.open(relativeUrls.checkout);
        await checkoutPage.reduceItemCount(product.id, 1);
        await checkoutPage.assertCheckoutItemCountToBe(newTargetProductCount, product.id);
      });

      test('THEN cart items count should be updated', async ({ page }) => {
        const newTargetProductsCount = getTargetProductsCount(updatedProducts);

        const homePage = new HomePage(page);
        const shopPage = new ShopPage(page);
        const checkoutPage = new CheckoutPage(page);

        await homePage.open(relativeUrls.shop);
        await shopPage.addProductsToCart(targetProducts);
        await shopPage.open(relativeUrls.checkout);
        await checkoutPage.reduceItemCount(product.id, 1);
        await expect(checkoutPage.cartItemsCounter).toHaveText(newTargetProductsCount.toString());
        await checkoutPage.openCart();
        await checkoutPage.assertCartItems(updatedProducts);
      });
    });

    test.describe('AND removes item completely', () => {
      test.beforeEach(() => {
        targetProducts = getProductsMap(previewProducts, productsToAddCount);
        [ product ] = targetProducts;
        updatedProducts = getUpdatedProducts('remove', targetProducts, product);
      });

      test('THEN checkout total should be updated', async ({ page }) => {
        const updatedTotalText = `Total: $${getCartTotal(updatedProducts)}`;

        const homePage = new HomePage(page);
        const shopPage = new ShopPage(page);
        const checkoutPage = new CheckoutPage(page);

        await homePage.open(relativeUrls.shop);
        await shopPage.addProductsToCart(targetProducts);
        await shopPage.open(relativeUrls.checkout);
        await checkoutPage.reduceItemCount(product.id, product.count);
        await expect(checkoutPage.total).toHaveText(updatedTotalText);
      });

      test('THEN checkout item row should not exist', async ({ page }) => {
        const homePage = new HomePage(page);
        const shopPage = new ShopPage(page);
        const checkoutPage = new CheckoutPage(page);

        await homePage.open(relativeUrls.shop);
        await shopPage.addProductsToCart(targetProducts);
        await shopPage.open(relativeUrls.checkout);
        await checkoutPage.reduceItemCount(product.id, product.count);
        await checkoutPage.assertCheckoutItemNotExist(product.id);
      });

      test('THEN cart items count should be updated', async ({ page }) => {
        const newTargetProductsCount = getTargetProductsCount(updatedProducts);

        const homePage = new HomePage(page);
        const shopPage = new ShopPage(page);
        const checkoutPage = new CheckoutPage(page);

        await homePage.open(relativeUrls.shop);
        await shopPage.addProductsToCart(targetProducts);
        await shopPage.open(relativeUrls.checkout);
        await checkoutPage.reduceItemCount(product.id, product.count);
        await expect(checkoutPage.cartItemsCounter).toHaveText(newTargetProductsCount.toString());
        await checkoutPage.openCart();
        await checkoutPage.assertCartItems(updatedProducts);
      });
    });
  });

  test.describe('WHEN a user removes item', () => {
    test.beforeEach(() => {
      targetProducts = getProductsMap(previewProducts, productsToAddCount);
      [ product ] = targetProducts;
      updatedProducts = getUpdatedProducts('remove', targetProducts, product);
    });

    test('THEN checkout total should be updated', async ({ page }) => {
      const updatedTotalText = `Total: $${getCartTotal(updatedProducts)}`;

      const homePage = new HomePage(page);
      const shopPage = new ShopPage(page);
      const checkoutPage = new CheckoutPage(page);

      await homePage.open(relativeUrls.shop);
      await shopPage.addProductsToCart(targetProducts);
      await shopPage.open(relativeUrls.checkout);
      await checkoutPage.removeItem(product.id);
      await expect(checkoutPage.total).toHaveText(updatedTotalText);
    });

    test('THEN checkout item row count should not exist', async ({ page }) => {
      const homePage = new HomePage(page);
      const shopPage = new ShopPage(page);
      const checkoutPage = new CheckoutPage(page);

      await homePage.open(relativeUrls.shop);
      await shopPage.addProductsToCart(targetProducts);
      await shopPage.open(relativeUrls.checkout);
      await checkoutPage.removeItem(product.id);
      await checkoutPage.assertCheckoutItemNotExist();
    });

    test('THEN cart items count should be updated', async ({ page }) => {
      const newTargetProductsCount = getTargetProductsCount(updatedProducts);

      const homePage = new HomePage(page);
      const shopPage = new ShopPage(page);
      const checkoutPage = new CheckoutPage(page);

      await homePage.open(relativeUrls.shop);
      await shopPage.addProductsToCart(targetProducts);
      await shopPage.open(relativeUrls.checkout);
      await checkoutPage.removeItem(product.id);
      await expect(checkoutPage.cartItemsCounter).toHaveText(newTargetProductsCount.toString());
      await checkoutPage.openCart();
      await checkoutPage.assertCartItems(updatedProducts);
    });
  });
});
