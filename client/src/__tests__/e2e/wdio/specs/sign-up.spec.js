import { expect } from '@wdio/globals';
import { relativeUrls } from '../../common/relative-urls';
import User from '../../common/entities/user';
import SigningPage from '../pages/signing.page';
import errors from '../../common/errors';

describe('Sign Up', () => {
  let userData;
  const signupPage = new SigningPage();

  beforeEach(async () => {
    userData = new User();

    await signupPage.open(relativeUrls.signing);
  });

  it('user should be able to sign up with valid data', async () => {
    await signupPage.signUp(userData);
    await expect(signupPage.signOutButton).toBeDisplayed();
  });

  it('user should not be able to sign up with already registered email', async () => {
    await signupPage.signUp(userData);
    await expect(signupPage.signOutButton).toBeDisplayed();
    await signupPage.signOutButton.click();
    await expect(signupPage.signInButton).toBeDisplayed();

    await signupPage.open(relativeUrls.signing);
    await signupPage.signUp(userData);
    await expect(signupPage.errorMessage).toBeDisplayed();
    await expect(signupPage.errorMessage).toHaveText(errors.alreadyRegisteredEmail);
  });

  it('user should not be able to sign up with missing name', async () => {
    userData.name = null;

    await signupPage.signUp(userData);
    await expect(signupPage.failedSignUpNameField).toBeDisplayed();
  });

  it('user should not be able to sign up with missing email', async () => {
    userData.email = null;

    await signupPage.signUp(userData);
    await expect(signupPage.failedSignUpEmailField).toBeDisplayed();
  });

  it('user should not be able to sign up with missing password', async () => {
    userData.password = null;

    await signupPage.signUp(userData);
    await expect(signupPage.failedSignUpPasswordField).toBeDisplayed();
  });

  it('user should not be able to sign up with missing confirmation password', async () => {
    userData.confirmPassword = null;

    await signupPage.signUp(userData);
    await expect(signupPage.failedSignUpConfirmPasswordField).toBeDisplayed();
  });

  it("user should not be able to sign up if confirmation password doesn't match", async () => {
    userData.confirmPassword = 'qwerty123';

    await signupPage.signUp(userData);
    await expect(await signupPage.getAlertText()).toBe(errors.passwordDoesNotMatch);
  });

  it.skip('user should not be able to sign up with invalid name', async () => {
    userData.name = 'a';

    await signupPage.signUp(userData);
  });

  it('user should not be able to sign up with invalid email', async () => {
    userData.email = userData.email.split('@')[0];

    await signupPage.signUp(userData);
    await expect(signupPage.failedSignUpEmailField).toBeDisplayed();
  });

  it('user should not be able to sign up with invalid password', async () => {
    const tooShortPassword = 'qwert';
    userData.password = tooShortPassword;
    userData.confirmPassword = tooShortPassword;

    await signupPage.signUp(userData);
    await expect(signupPage.errorMessage).toBeDisplayed();
    await expect(signupPage.errorMessage).toHaveText(errors.tooShortPassword);
  });

  it('user should not be able to sign up with submitting empty form', async () => {
    await signupPage.submitButton.click();
    await expect(signupPage.failedSignUpNameField).toBeDisplayed();
  });
});
