import User from '../../common/entities/user';
import SigningPage from '../pages/signing.page';
import { relativeUrls } from '../../common/relative-urls';
import errors from '../../common/errors';

const { test, expect } = require('@playwright/test');

test.describe('Sign Up', () => {
  let userData;

  test.beforeEach(() => {
    userData = new User();
  });

  test('user should be able to sign up with valid data', async ({ page }) => {
    const signUpPage = new SigningPage(page);

    await signUpPage.open(relativeUrls.signing);
    await signUpPage.signUp(userData);
    await expect(signUpPage.signOutButton).toBeVisible();
  });

  test('user should not be able to sign up with already registered email', async ({ page }) => {
    const signUpPage = new SigningPage(page);

    await signUpPage.open(relativeUrls.signing);
    await signUpPage.signUp(userData);
    await expect(signUpPage.signOutButton).toBeVisible();
    await signUpPage.signOutButton.click();
    await expect(signUpPage.signInButton).toBeVisible();
    await signUpPage.open(relativeUrls.signing);
    await signUpPage.signUp(userData);
    await expect(signUpPage.errorMessage).toBeVisible();
    await expect(signUpPage.errorMessage).toHaveText(errors.alreadyRegisteredEmail);
  });

  test('user should not be able to sign up with missing name', async ({ page }) => {
    const signUpPage = new SigningPage(page);
    userData.name = null;

    await signUpPage.open(relativeUrls.signing);
    await signUpPage.signUp(userData);
    await expect(signUpPage.failedNameInput).toBeVisible();
  });

  test('user should not be able to sign up with missing email', async ({ page }) => {
    const signUpPage = new SigningPage(page);
    userData.email = null;

    await signUpPage.open(relativeUrls.signing);
    await signUpPage.signUp(userData);
    await expect(signUpPage.failedEmailInput).toBeVisible();
  });

  test('user should not be able to sign up with missing password', async ({ page }) => {
    const signUpPage = new SigningPage(page);
    userData.password = null;

    await signUpPage.open(relativeUrls.signing);
    await signUpPage.signUp(userData);
    await expect(signUpPage.failedPasswordInput).toBeVisible();
  });

  test('user should not be able to sign up with missing confirmation password', async ({ page }) => {
    const signUpPage = new SigningPage(page);
    userData.confirmPassword = null;

    await signUpPage.open(relativeUrls.signing);
    await signUpPage.signUp(userData);
    await expect(signUpPage.failedConfirmPasswordInput).toBeVisible();
  });

  test("user should not be able to sign up if confirmation password doesn't match", async ({ page }) => {
    const signUpPage = new SigningPage(page);
    userData.confirmPassword = 'qwerty123';

    await signUpPage.open(relativeUrls.signing);
    await signUpPage.signUp(userData);

    page.on('dialog', async (dialog) => {
      expect(dialog.type()).toContain('alert');
      expect(dialog.message()).toContain(errors.passwordDoesNotMatch);
    });
  });

  test.skip('user should not be able to sign up with invalid name', async ({ page }) => {
    const signUpPage = new SigningPage(page);
    userData.name = '   a   ';

    await signUpPage.open(relativeUrls.signing);
    await signUpPage.signUp(userData);
    await expect(signUpPage.errorMessage).toBeVisible();
  });

  test('user should not be able to sign up with invalid email', async ({ page }) => {
    const signUpPage = new SigningPage(page);
    userData.email = userData.email.split('@')[0];

    await signUpPage.open(relativeUrls.signing);
    await signUpPage.signUp(userData);
    await expect(signUpPage.failedEmailInput).toBeVisible();
  });

  test('user should not be able to sign up with invalid password', async ({ page }) => {
    const signUpPage = new SigningPage(page);
    userData.password = 'qwert';
    userData.confirmPassword = 'qwert';

    await signUpPage.open(relativeUrls.signing);
    await signUpPage.signUp(userData);
    await expect(signUpPage.errorMessage).toBeVisible();
    await expect(signUpPage.errorMessage).toHaveText(errors.tooShortPassword);
  });

  test('user should not be able to sign up with submitting empty form', async ({ page }) => {
    const signUpPage = new SigningPage(page);

    await signUpPage.open(relativeUrls.signing);
    await signUpPage.submitButton.click();
    await expect(signUpPage.failedNameInput).toBeVisible();
  });
});
