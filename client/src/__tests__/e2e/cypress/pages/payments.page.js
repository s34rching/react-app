import BasePage from './basepage.page';

class PaymentsPage extends BasePage {
  get paymentsPopUp() {
    return cy.getPaymentsIframe();
  }

  get emailField() {
    return this.paymentsPopUp.find('#email');
  }

  get shippingNameField() {
    return this.paymentsPopUp.find('#shipping-name');
  }

  get shippingAddressField() {
    return this.paymentsPopUp.find('#shipping-street');
  }

  get shippingPostalCodeField() {
    return this.paymentsPopUp.find('#shipping-zip');
  }

  get shippingCityField() {
    return this.paymentsPopUp.find('#shipping-city');
  }

  get shippingCountrySelect() {
    return this.paymentsPopUp.find('#shipping-country');
  }

  get cardNumberField() {
    return this.paymentsPopUp.find('#card_number');
  }

  get cardExpirationDateField() {
    return this.paymentsPopUp.find('#cc-exp');
  }

  get cvcField() {
    return this.paymentsPopUp.find('#cc-csc');
  }

  get closeIcon() {
    return this.paymentsPopUp.find('a.close');
  }

  get paymentsTitle() {
    return this.paymentsPopUp.find('.title');
  }

  get sameAddressCheckbox() {
    return this.paymentsPopUp.find('.checkbox.same-address');
  }

  get invalidInputField() {
    return this.paymentsPopUp.find('input.invalid');
  }

  get billingNameField() {
    return this.paymentsPopUp.find('#billing-name');
  }

  get billingAddressField() {
    return this.paymentsPopUp.find('#billing-street');
  }

  get billingPostalCodeField() {
    return this.paymentsPopUp.find('#billing-zip');
  }

  get billingCityField() {
    return this.paymentsPopUp.find('#billing-city');
  }

  get billingCountrySelect() {
    return this.paymentsPopUp.find('#billing-country');
  }

  get backArrow() {
    return this.paymentsPopUp.find('a.back');
  }

  changeCountry(targetCountry) {
    return this.shippingCountrySelect.select(targetCountry);
  }

  changeBillingCountry(targetCountry) {
    return this.billingCountrySelect.select(targetCountry);
  }

  proceedToNextStep() {
    return this.paymentsPopUp.find('#submitButton').click();
  }

  enterPersonalData(userData) {
    this.emailField.should('be.visible');
    this.changeCountry(userData.country);
    if (userData.email) {
      this.emailField.type(userData.email);
    }
    if (userData.name) {
      this.shippingNameField.type(userData.name);
    }
    if (userData.line1) {
      this.shippingAddressField.type(userData.line1);
    }
    if (userData.zip) {
      this.shippingPostalCodeField.type(userData.zip);
    }
    if (userData.city) {
      this.shippingCityField.clear().type(userData.city);
    }
    if (userData.billingAddress) {
      this.sameAddressCheckbox.click();
      this.paymentsPopUp.contains('Billing').click();
      this.billingNameField.should('be.visible');
      this.changeBillingCountry(userData.billingAddress.country);
      if (userData.billingAddress.name) {
        this.billingNameField.clear().type(userData.billingAddress.name);
      } else {
        this.billingNameField.clear();
      }
      if (userData.billingAddress.line1) {
        this.billingAddressField.clear().type(userData.billingAddress.line1);
      } else {
        this.billingAddressField.clear();
      }
      if (userData.billingAddress.zip) {
        this.billingPostalCodeField.clear().type(userData.billingAddress.zip);
      } else {
        this.billingPostalCodeField.clear();
      }
      if (userData.billingAddress.city) {
        this.billingCityField.clear().type(userData.billingAddress.city);
      } else {
        this.billingCityField.clear();
      }
    }
    this.proceedToNextStep();
  }

  enterCardData(userData) {
    if (userData.number) {
      this.cardNumberField.type(userData.number);
    }
    if (userData.expDate) {
      this.cardExpirationDateField.type(userData.expDate);
    }
    if (userData.cvv) {
      this.cvcField.type(userData.cvv);
    }
    this.proceedToNextStep();
  }

  closePaymentPopUp() {
    this.closeIcon.should('be.visible').click();
  }

  backToPersonalDataForm() {
    this.backArrow.should('be.visible');
    this.backArrow.click();
  }
}

export default PaymentsPage;
