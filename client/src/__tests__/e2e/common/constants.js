const constants = {
  MAX_ITEMS_COUNT: 10,
  PREVIEW_ITEMS_NUMBER: 4,
  SMM_IMAGE_WIDTH: 48,
  SMM_IMAGE_HEIGHT: 48,
  EXTERNAL_LINKS_TARGET: '_blank',
};

module.exports = constants;
