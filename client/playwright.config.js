const config = {
  workers: 1,
  expect: { timeout: 20000 },
  reporter: [
    [ 'list' ],
    [ 'html', {
      outputFolder: './src/__tests__/e2e/reports/playwright/html',
      open: 'never',
    } ] ],
  outputDir: './src/__tests__/e2e/reports/playwright/screenshots',
  use: {
    actionTimeout: 20000,
    baseURL: 'http://localhost:3000',
    screenshot: 'only-on-failure',
    headless: true,
    ignoreHTTPSErrors: true,
    testDir: 'src/__tests__/e2e/playwright/specs',
  },
};

module.exports = config;
