const { browser } = require('@wdio/globals');

exports.config = {
  runner: 'local',
  specs: [
    './src/__tests__/e2e/wdio/specs/**/*.js',
  ],
  exclude: [],
  maxInstances: 1,
  capabilities: [ {
    maxInstances: 1,
    browserName: 'chrome',
    'goog:chromeOptions': {
      args: [
        'headless',
        '--disable-gpu',
        '--no-sandbox',
        '--window-size=1920,1080',
        '--disable-dev-shm-usage',
      ],
    },
    acceptInsecureCerts: true,
  } ],
  logLevel: 'info',
  bail: 0,
  baseUrl: 'http://localhost:3000',
  waitforTimeout: 20000,
  connectionRetryTimeout: 120000,
  connectionRetryCount: 3,
  services: [ 'chromedriver' ],
  framework: 'mocha',
  mochaOpts: {
    ui: 'bdd',
    timeout: 60000,
  },
  reporters: [
    'spec',
    [ 'allure', {
      outputDir: './src/__tests__/e2e/reports/wdio/results',
      disableWebdriverStepsReporting: true,
      disableWebdriverScreenshotsReporting: false,
    } ],
  ],
  afterTest: async (test, context, { error }) => {
    if (error) {
      await browser.takeScreenshot();
    }
    await browser.execute('window.localStorage.clear()');
  },
};
