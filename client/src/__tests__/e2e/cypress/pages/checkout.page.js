import { times } from 'lodash';
import BasePage from './basepage.page';

class CheckoutPage extends BasePage {
  get total() {
    return cy.get('[data-test="checkout-total"]');
  }

  get checkoutItems() {
    return cy.get('[data-test^="checkout-item-"]');
  }

  getCheckoutItem(id) {
    return cy.get(`[data-test=checkout-item-${id}]`);
  }

  getCheckoutItemIncreaseCountButton(id) {
    return this.getCheckoutItem(id).find('[data-test=item-increase-count]');
  }

  getCheckoutItemReduceCountButton(id) {
    return this.getCheckoutItem(id).find('[data-test=item-reduce-count]');
  }

  getCheckoutItemRemoveButton(id) {
    return this.getCheckoutItem(id).find('[data-test=item-remove]');
  }

  getCheckoutItemCounter(id) {
    return this.getCheckoutItem(id).find('[data-test=item-counter]');
  }

  increaseItemCount(id, count) {
    times(count, () => {
      this.getCheckoutItemIncreaseCountButton(id).click();
    });
  }

  reduceItemCount(id, count) {
    times(count, () => {
      this.getCheckoutItemReduceCountButton(id).click();
    });
  }

  removeItem(id) {
    this.getCheckoutItemRemoveButton(id).click();
  }

  openPayments() {
    return cy.get('button').contains('Pay Now').click();
  }
}

export default CheckoutPage;
