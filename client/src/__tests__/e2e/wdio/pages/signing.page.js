import { $ } from '@wdio/globals';
import BasePage from './base.page';

class SigningPage extends BasePage {
  get signInEmail() {
    return $("[data-test='sign-in-email']");
  }

  get signInPassword() {
    return $("[data-test='sign-in-password']");
  }

  get failedSignInEmailField() {
    return $("[data-test='sign-in-email'][required]:invalid");
  }

  get failedSignInPasswordField() {
    return $("[data-test='sign-in-password'][required]:invalid");
  }

  get submitSignInButton() {
    return $("[data-test='sign-in-submit']");
  }

  get signUpName() {
    return $("[data-test='sign-up-name']");
  }

  get signUpEmail() {
    return $("[data-test='sign-up-email']");
  }

  get signUpPassword() {
    return $("[data-test='sign-up-password']");
  }

  get signUpConfirmPassword() {
    return $("[data-test='sign-up-confirm-password']");
  }

  get submitButton() {
    return $("[data-test='sign-up-submit']");
  }

  get errorMessage() {
    return $("[data-test='signing-error']");
  }

  get failedSignUpNameField() {
    return $("[data-test='sign-up-name'][required]:invalid");
  }

  get failedSignUpEmailField() {
    return $("[data-test='sign-up-email'][required]:invalid");
  }

  get failedSignUpPasswordField() {
    return $("[data-test='sign-up-password'][required]:invalid");
  }

  get failedSignUpConfirmPasswordField() {
    return $("[data-test='sign-up-confirm-password'][required]:invalid");
  }

  async signIn({ email, password }) {
    if (email) {
      await this.signInEmail.setValue(email);
    }
    if (password) {
      await this.signInPassword.setValue(password);
    }
    await this.submitSignInButton.click();
  }

  async signUp({ name, email, password, confirmPassword = password }) {
    if (name) {
      await this.signUpName.setValue(name);
    }
    if (email) {
      await this.signUpEmail.setValue(email);
    }
    if (password) {
      await this.signUpPassword.setValue(password);
    }
    if (confirmPassword) {
      await this.signUpConfirmPassword.setValue(confirmPassword);
    }
    await this.submitButton.click();
  }
}

export default SigningPage;
