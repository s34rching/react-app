import { test, expect } from '@playwright/test';
import dayjs from 'dayjs';
import HomePage from '../pages/home.page';
import ShopPage from '../pages/shop.page';
import CheckoutPage from '../pages/checkout.page';
import PaymentsPage from '../pages/payments.page';
import { relativeUrls } from '../../common/relative-urls';
import { getCollectionProducts, getRandomCollection } from '../../common/data-providers';
import { getCartTotal, getProductsMap } from '../../common/data-handlers';
import Address from '../../common/entities/address';
import BillingAddress from '../../common/entities/billing-address';
import PaymentCard from '../../common/entities/payment-card';
import User from '../../common/entities/user';
import errors from '../../common/errors';

test.describe('Payments', () => {
  let userData;

  const productsToAddCount = 2;
  const randomCollection = getRandomCollection();
  const previewProducts = getCollectionProducts(randomCollection, true);
  const previewTargetProducts = getProductsMap(previewProducts, productsToAddCount);

  test.beforeEach(() => {
    userData = {
      ...new User(),
      ...new Address(),
      ...new PaymentCard(),
    };
  });

  test('user should be able to pay for purchase', async ({ page }) => {
    const homePage = new HomePage(page);
    const shopPage = new ShopPage(page);
    const checkoutPage = new CheckoutPage(page);
    const paymentsPage = new PaymentsPage(page);

    await homePage.open(relativeUrls.shop);
    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.goToCheckout();
    await checkoutPage.openPayments();
    await paymentsPage.enterPersonalData(userData);
    await paymentsPage.enterCardData(userData);
    page.on('dialog', async (dialog) => {
      expect(dialog.type()).toContain('alert');
      expect(dialog.message()).toContain(errors.chargeError);
    });
  });

  test('user should be able to pay for purchases if billing address is valid', async ({ page }) => {
    userData.billingAddress = new BillingAddress();

    const homePage = new HomePage(page);
    const shopPage = new ShopPage(page);
    const checkoutPage = new CheckoutPage(page);
    const paymentsPage = new PaymentsPage(page);

    await homePage.open(relativeUrls.shop);
    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.goToCheckout();
    await checkoutPage.openPayments();
    await paymentsPage.enterPersonalData(userData);
    await paymentsPage.enterCardData(userData);
    page.on('dialog', async (dialog) => {
      expect(dialog.type()).toContain('alert');
      expect(dialog.message()).toContain(errors.chargeError);
    });
  });

  test('user should be able to return to previous screen', async ({ page }) => {
    const homePage = new HomePage(page);
    const shopPage = new ShopPage(page);
    const checkoutPage = new CheckoutPage(page);
    const paymentsPage = new PaymentsPage(page);

    await homePage.open(relativeUrls.shop);
    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.goToCheckout();
    await checkoutPage.openPayments();
    await paymentsPage.enterPersonalData(userData);
    await expect(paymentsPage.cardNumberField).toBeVisible();
    await paymentsPage.backToPersonalDataForm();
    await expect(paymentsPage.shippingNameField).toBeVisible();
  });

  test('user should be able to close payments modal', async ({ page }) => {
    const homePage = new HomePage(page);
    const shopPage = new ShopPage(page);
    const checkoutPage = new CheckoutPage(page);
    const paymentsPage = new PaymentsPage(page);

    await homePage.open(relativeUrls.shop);
    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.goToCheckout();
    await checkoutPage.openPayments();
    await paymentsPage.closePaymentsPopUp();
    await expect(checkoutPage.total).toBeVisible();
  });

  test('cart total should be equal to payment total', async ({ page }) => {
    const total = getCartTotal(previewTargetProducts).toString();

    const homePage = new HomePage(page);
    const shopPage = new ShopPage(page);
    const checkoutPage = new CheckoutPage(page);
    const paymentsPage = new PaymentsPage(page);

    await homePage.open(relativeUrls.shop);
    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.goToCheckout();
    await expect(checkoutPage.total).toContainText(total);
    await checkoutPage.openPayments();
    await expect(paymentsPage.paymentsTitle).toContainText(total);
  });

  test('shipping address should be the same as billing address by default', async ({ page }) => {
    const homePage = new HomePage(page);
    const shopPage = new ShopPage(page);
    const checkoutPage = new CheckoutPage(page);
    const paymentsPage = new PaymentsPage(page);

    await homePage.open(relativeUrls.shop);
    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.goToCheckout();
    await checkoutPage.openPayments();
    await expect(paymentsPage.sameAddressCheckbox).toBeVisible();
    await expect(paymentsPage.sameAddressCheckbox).toHaveClass('checkbox same-address checked');
  });

  test('user should not be able to proceed if entered invalid email', async ({ page }) => {
    userData.email = 'email.com';

    const homePage = new HomePage(page);
    const shopPage = new ShopPage(page);
    const checkoutPage = new CheckoutPage(page);
    const paymentsPage = new PaymentsPage(page);

    await homePage.open(relativeUrls.shop);
    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.goToCheckout();
    await checkoutPage.openPayments();
    await paymentsPage.enterPersonalData(userData);
    await expect(paymentsPage.invalidInputField).toBeVisible();
    await expect(paymentsPage.invalidInputField).toHaveAttribute('id', 'email');
  });

  test('user should not be able to proceed with missing email', async ({ page }) => {
    userData.email = null;

    const homePage = new HomePage(page);
    const shopPage = new ShopPage(page);
    const checkoutPage = new CheckoutPage(page);
    const paymentsPage = new PaymentsPage(page);

    await homePage.open(relativeUrls.shop);
    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.goToCheckout();
    await checkoutPage.openPayments();
    await paymentsPage.enterPersonalData(userData);
    await expect(paymentsPage.invalidInputField).toBeVisible();
    await expect(paymentsPage.invalidInputField).toHaveAttribute('id', 'email');
  });

  test('user should not be able to proceed with missing name', async ({ page }) => {
    userData.name = null;

    const homePage = new HomePage(page);
    const shopPage = new ShopPage(page);
    const checkoutPage = new CheckoutPage(page);
    const paymentsPage = new PaymentsPage(page);

    await homePage.open(relativeUrls.shop);
    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.goToCheckout();
    await checkoutPage.openPayments();
    await paymentsPage.enterPersonalData(userData);
    await expect(paymentsPage.invalidInputField).toBeVisible();
    await expect(paymentsPage.invalidInputField).toHaveAttribute('id', 'shipping-name');
  });

  test('user should not be able to proceed with missing street address', async ({ page }) => {
    userData.line1 = null;

    const homePage = new HomePage(page);
    const shopPage = new ShopPage(page);
    const checkoutPage = new CheckoutPage(page);
    const paymentsPage = new PaymentsPage(page);

    await homePage.open(relativeUrls.shop);
    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.goToCheckout();
    await checkoutPage.openPayments();
    await paymentsPage.enterPersonalData(userData);
    await expect(paymentsPage.invalidInputField).toBeVisible();
    await expect(paymentsPage.invalidInputField).toHaveAttribute('id', 'shipping-street');
  });

  test('user should not be able to proceed with missing postal code', async ({ page }) => {
    userData.zip = null;

    const homePage = new HomePage(page);
    const shopPage = new ShopPage(page);
    const checkoutPage = new CheckoutPage(page);
    const paymentsPage = new PaymentsPage(page);

    await homePage.open(relativeUrls.shop);
    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.goToCheckout();
    await checkoutPage.openPayments();
    await paymentsPage.enterPersonalData(userData);
    await expect(paymentsPage.invalidInputField).toBeVisible();
    await expect(paymentsPage.invalidInputField).toHaveAttribute('id', 'shipping-zip');
  });

  test('user city should be defined by default if zip code was entered', async ({ page }) => {
    userData.city = null;

    const homePage = new HomePage(page);
    const shopPage = new ShopPage(page);
    const checkoutPage = new CheckoutPage(page);
    const paymentsPage = new PaymentsPage(page);

    await homePage.open(relativeUrls.shop);
    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.goToCheckout();
    await checkoutPage.openPayments();
    await paymentsPage.enterPersonalData(userData);
    await expect(paymentsPage.cardNumberField).toBeVisible();
  });

  test('user should not be able to proceed if invalid card number was entered', async ({ page }) => {
    userData.number = new PaymentCard().number.slice(0, 5);

    const homePage = new HomePage(page);
    const shopPage = new ShopPage(page);
    const checkoutPage = new CheckoutPage(page);
    const paymentsPage = new PaymentsPage(page);

    await homePage.open(relativeUrls.shop);
    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.goToCheckout();
    await checkoutPage.openPayments();
    await paymentsPage.enterPersonalData(userData);
    await paymentsPage.enterCardData(userData);
    await expect(paymentsPage.invalidInputField).toBeVisible();
    await expect(paymentsPage.invalidInputField).toHaveAttribute('id', 'card_number');
  });

  test('user should not be able to proceed if card number is missing', async ({ page }) => {
    userData.number = null;

    const homePage = new HomePage(page);
    const shopPage = new ShopPage(page);
    const checkoutPage = new CheckoutPage(page);
    const paymentsPage = new PaymentsPage(page);

    await homePage.open(relativeUrls.shop);
    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.goToCheckout();
    await checkoutPage.openPayments();
    await paymentsPage.enterPersonalData(userData);
    await paymentsPage.enterCardData(userData);
    await expect(paymentsPage.invalidInputField).toBeVisible();
    await expect(paymentsPage.invalidInputField).toHaveAttribute('id', 'card_number');
  });

  test('user should not be able to proceed if card is expired', async ({ page }) => {
    userData.expDate = dayjs().subtract(1, 'month').format('MMYY');

    const homePage = new HomePage(page);
    const shopPage = new ShopPage(page);
    const checkoutPage = new CheckoutPage(page);
    const paymentsPage = new PaymentsPage(page);

    await homePage.open(relativeUrls.shop);
    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.goToCheckout();
    await checkoutPage.openPayments();
    await paymentsPage.enterPersonalData(userData);
    await paymentsPage.enterCardData(userData);
    await expect(paymentsPage.invalidInputField).toBeVisible();
    await expect(paymentsPage.invalidInputField).toHaveAttribute('id', 'cc-exp');
  });

  test('user should not be able to proceed if card expiration date is invalid', async ({ page }) => {
    userData.expDate = '9999';

    const homePage = new HomePage(page);
    const shopPage = new ShopPage(page);
    const checkoutPage = new CheckoutPage(page);
    const paymentsPage = new PaymentsPage(page);

    await homePage.open(relativeUrls.shop);
    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.goToCheckout();
    await checkoutPage.openPayments();
    await paymentsPage.enterPersonalData(userData);
    await paymentsPage.enterCardData(userData);
    await expect(paymentsPage.invalidInputField).toBeVisible();
    await expect(paymentsPage.invalidInputField).toHaveAttribute('id', 'cc-exp');
  });

  test('user should not be able to proceed if card expiration date is missing', async ({ page }) => {
    userData.expDate = null;

    const homePage = new HomePage(page);
    const shopPage = new ShopPage(page);
    const checkoutPage = new CheckoutPage(page);
    const paymentsPage = new PaymentsPage(page);

    await homePage.open(relativeUrls.shop);
    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.goToCheckout();
    await checkoutPage.openPayments();
    await paymentsPage.enterPersonalData(userData);
    await paymentsPage.enterCardData(userData);
    await expect(paymentsPage.invalidInputField).toBeVisible();
    await expect(paymentsPage.invalidInputField).toHaveAttribute('id', 'cc-exp');
  });

  test('user should not be able to proceed if cvv is invalid', async ({ page }) => {
    userData.cvv = '1';

    const homePage = new HomePage(page);
    const shopPage = new ShopPage(page);
    const checkoutPage = new CheckoutPage(page);
    const paymentsPage = new PaymentsPage(page);

    await homePage.open(relativeUrls.shop);
    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.goToCheckout();
    await checkoutPage.openPayments();
    await paymentsPage.enterPersonalData(userData);
    await paymentsPage.enterCardData(userData);
    await expect(paymentsPage.invalidInputField).toBeVisible();
    await expect(paymentsPage.invalidInputField).toHaveAttribute('id', 'cc-csc');
  });

  test('user should not be able to proceed if cvv is missing', async ({ page }) => {
    userData.cvv = null;

    const homePage = new HomePage(page);
    const shopPage = new ShopPage(page);
    const checkoutPage = new CheckoutPage(page);
    const paymentsPage = new PaymentsPage(page);

    await homePage.open(relativeUrls.shop);
    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.goToCheckout();
    await checkoutPage.openPayments();
    await paymentsPage.enterPersonalData(userData);
    await paymentsPage.enterCardData(userData);
    await expect(paymentsPage.invalidInputField).toBeVisible();
    await expect(paymentsPage.invalidInputField).toHaveAttribute('id', 'cc-csc');
  });

  test('user should not be able to pay for purchases if billing name is missing', async ({ page }) => {
    const homePage = new HomePage(page);
    const shopPage = new ShopPage(page);
    const checkoutPage = new CheckoutPage(page);
    const paymentsPage = new PaymentsPage(page);

    await homePage.open(relativeUrls.shop);
    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.goToCheckout();
    await checkoutPage.openPayments();
    await paymentsPage.enterPersonalData(userData);
    await paymentsPage.backToPersonalDataForm();
    await paymentsPage.openBillingAddressForm();
    await paymentsPage.clearInput('billing-name');
    await paymentsPage.proceedToNextStep();
    await expect(paymentsPage.invalidInputField).toBeVisible();
    await expect(paymentsPage.invalidInputField).toHaveAttribute('id', 'billing-name');
  });

  test('user should not be able to pay for purchases if billing address is missing', async ({ page }) => {
    userData.billingAddress = new BillingAddress();
    userData.billingAddress.line1 = null;

    const homePage = new HomePage(page);
    const shopPage = new ShopPage(page);
    const checkoutPage = new CheckoutPage(page);
    const paymentsPage = new PaymentsPage(page);

    await homePage.open(relativeUrls.shop);
    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.goToCheckout();
    await checkoutPage.openPayments();
    await paymentsPage.enterPersonalData(userData);
    await expect(paymentsPage.invalidInputField).toBeVisible();
    await expect(paymentsPage.invalidInputField).toHaveAttribute('id', 'billing-street');
  });

  test('user should not be able to pay for purchases if billing postal code is missing', async ({ page }) => {
    userData.billingAddress = new BillingAddress();
    userData.billingAddress.zip = null;

    const homePage = new HomePage(page);
    const shopPage = new ShopPage(page);
    const checkoutPage = new CheckoutPage(page);
    const paymentsPage = new PaymentsPage(page);

    await homePage.open(relativeUrls.shop);
    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.goToCheckout();
    await checkoutPage.openPayments();
    await paymentsPage.enterPersonalData(userData);
    await expect(paymentsPage.invalidInputField).toBeVisible();
    await expect(paymentsPage.invalidInputField).toHaveAttribute('id', 'billing-zip');
  });

  test('user should not be able to pay for purchases if billing city is missing', async ({ page }) => {
    userData.billingAddress = new BillingAddress();
    userData.billingAddress.city = null;

    const homePage = new HomePage(page);
    const shopPage = new ShopPage(page);
    const checkoutPage = new CheckoutPage(page);
    const paymentsPage = new PaymentsPage(page);

    await homePage.open(relativeUrls.shop);
    await shopPage.addProductsToCart(previewTargetProducts);
    await shopPage.goToCheckout();
    await checkoutPage.openPayments();
    await paymentsPage.enterPersonalData(userData);
    await expect(paymentsPage.invalidInputField).toBeVisible();
    await expect(paymentsPage.invalidInputField).toHaveAttribute('id', 'billing-city');
  });
});
