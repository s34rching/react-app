import BasePage from './base.page';

class SearchResultsPage extends BasePage {
  constructor(page) {
    super(page);
    this.page = page;
    this.emptyResultsContainer = page.locator('p', { hasText: 'Nothing found...' });
    this.foundProductsCards = page.locator('[data-test^=item-container-]');
  }
}

export default SearchResultsPage;
