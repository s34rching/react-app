import dayjs from 'dayjs';
import HomePage from '../pages/homepage.page';
import ShopPage from '../pages/shop.page';
import CheckoutPage from '../pages/checkout.page';
import PaymentsPage from '../pages/payments.page';
import { relativeUrls } from '../../common/relative-urls';
import { getCollectionProducts, getRandomCollection } from '../../common/data-providers';
import { getCartTotal, getProductsMap } from '../../common/data-handlers';
import Address from '../../common/entities/address';
import BillingAddress from '../../common/entities/billing-address';
import PaymentCard from '../../common/entities/payment-card';
import User from '../../common/entities/user';
import errors from '../../common/errors';

describe('Payments', () => {
  let userData;

  const homePage = new HomePage();
  const shopPage = new ShopPage();
  const checkoutPage = new CheckoutPage();
  const paymentsPage = new PaymentsPage();
  const productsToAddCount = 2;
  const randomCollection = getRandomCollection();
  const previewProducts = getCollectionProducts(randomCollection, true);
  const previewTargetProducts = getProductsMap(previewProducts, productsToAddCount);

  beforeEach(() => {
    userData = {
      ...new User(),
      ...new Address(),
      ...new PaymentCard(),
    };

    homePage.open(relativeUrls.shop);
  });

  it('user should be able to pay for purchases', () => {
    shopPage.addProductsToCart(previewTargetProducts);
    shopPage.open(relativeUrls.checkout);
    checkoutPage.openPayments();
    paymentsPage.enterPersonalData(userData);
    paymentsPage.enterCardData(userData);
    cy.on('window:alert', (text) => {
      expect(text).to.contains(errors.chargeError);
    });
  });

  it('user should be able to pay for purchases if billing address is valid', () => {
    userData.billingAddress = new BillingAddress();

    shopPage.addProductsToCart(previewTargetProducts);
    shopPage.open(relativeUrls.checkout);
    checkoutPage.openPayments();
    paymentsPage.enterPersonalData(userData);
    paymentsPage.enterCardData(userData);
    cy.on('window:alert', (text) => {
      expect(text).to.contains(errors.chargeError);
    });
  });

  it('user should be able to return to previous screen', () => {
    shopPage.addProductsToCart(previewTargetProducts);
    shopPage.open(relativeUrls.checkout);
    checkoutPage.openPayments();
    paymentsPage.enterPersonalData(userData);
    paymentsPage.cardNumberField.should('be.visible');
    paymentsPage.backToPersonalDataForm();
    paymentsPage.shippingNameField.should('be.visible');
  });

  it('user should be able to close payments modal', () => {
    shopPage.addProductsToCart(previewTargetProducts);
    shopPage.open(relativeUrls.checkout);
    checkoutPage.openPayments();
    paymentsPage.closePaymentPopUp();
    checkoutPage.total.should('be.visible');
  });

  it('cart total should be equal to payment total', () => {
    const total = getCartTotal(previewTargetProducts);

    shopPage.addProductsToCart(previewTargetProducts);
    shopPage.open(relativeUrls.checkout);
    checkoutPage.total.should('contain', total);
    checkoutPage.openPayments();
    paymentsPage.paymentsTitle.should('contain', total);
  });

  it('shipping address should be the same as billing address by default', () => {
    shopPage.addProductsToCart(previewTargetProducts);
    shopPage.open(relativeUrls.checkout);
    checkoutPage.openPayments();
    paymentsPage.sameAddressCheckbox.should('be.visible')
      .and('has.class', 'checked');
  });

  it('user should not be able to proceed if entered invalid email', () => {
    userData.email = 'email.com';

    shopPage.addProductsToCart(previewTargetProducts);
    shopPage.open(relativeUrls.checkout);
    checkoutPage.openPayments();
    paymentsPage.enterPersonalData(userData);
    paymentsPage.invalidInputField.should('be.visible')
      .and('have.attr', 'id', 'email');
  });

  it('user should not be able to proceed with missing email', () => {
    userData.email = null;

    shopPage.addProductsToCart(previewTargetProducts);
    shopPage.open(relativeUrls.checkout);
    checkoutPage.openPayments();
    paymentsPage.enterPersonalData(userData);
    paymentsPage.invalidInputField.should('be.visible')
      .and('have.attr', 'id', 'email');
  });

  it('user should not be able to proceed with missing name', () => {
    userData.name = null;

    shopPage.addProductsToCart(previewTargetProducts);
    shopPage.open(relativeUrls.checkout);
    checkoutPage.openPayments();
    paymentsPage.enterPersonalData(userData);
    paymentsPage.invalidInputField.should('be.visible')
      .and('have.attr', 'id', 'shipping-name');
  });

  it('user should not be able to proceed with missing street address', () => {
    userData.line1 = null;

    shopPage.addProductsToCart(previewTargetProducts);
    shopPage.open(relativeUrls.checkout);
    checkoutPage.openPayments();
    paymentsPage.enterPersonalData(userData);
    paymentsPage.invalidInputField.should('be.visible')
      .and('have.attr', 'id', 'shipping-street');
  });

  it('user should not be able to proceed with missing postal code', () => {
    userData.zip = null;

    shopPage.addProductsToCart(previewTargetProducts);
    shopPage.open(relativeUrls.checkout);
    checkoutPage.openPayments();
    paymentsPage.enterPersonalData(userData);
    paymentsPage.invalidInputField.should('be.visible')
      .and('have.attr', 'id', 'shipping-zip');
  });

  it('user city should be defined by default if zip code was entered', () => {
    userData.city = null;

    shopPage.addProductsToCart(previewTargetProducts);
    shopPage.open(relativeUrls.checkout);
    checkoutPage.openPayments();
    paymentsPage.enterPersonalData(userData);
    paymentsPage.cardNumberField.should('be.visible');
  });

  it('user should not be able to proceed if invalid card number was entered', () => {
    userData.number = new PaymentCard().number.slice(0, 5);

    shopPage.addProductsToCart(previewTargetProducts);
    shopPage.open(relativeUrls.checkout);
    checkoutPage.openPayments();
    paymentsPage.enterPersonalData(userData);
    paymentsPage.enterCardData(userData);
    paymentsPage.invalidInputField.should('be.visible')
      .and('have.attr', 'id', 'card_number');
  });

  it('user should not be able to proceed if card number is missing', () => {
    userData.number = ' ';

    shopPage.addProductsToCart(previewTargetProducts);
    shopPage.open(relativeUrls.checkout);
    checkoutPage.openPayments();
    paymentsPage.enterPersonalData(userData);
    paymentsPage.enterCardData(userData);
    paymentsPage.invalidInputField.should('be.visible')
      .and('have.attr', 'id', 'card_number');
  });

  it('user should not be able to proceed if card is expired', () => {
    userData.expDate = dayjs().subtract(1, 'month').format('MMYY');

    shopPage.addProductsToCart(previewTargetProducts);
    shopPage.open(relativeUrls.checkout);
    checkoutPage.openPayments();
    paymentsPage.enterPersonalData(userData);
    paymentsPage.enterCardData(userData);
    paymentsPage.invalidInputField.should('be.visible')
      .and('have.attr', 'id', 'cc-exp');
  });

  it('user should not be able to proceed if card expiration date is invalid', () => {
    userData.expDate = '9999';

    shopPage.addProductsToCart(previewTargetProducts);
    shopPage.open(relativeUrls.checkout);
    checkoutPage.openPayments();
    paymentsPage.enterPersonalData(userData);
    paymentsPage.enterCardData(userData);
    paymentsPage.invalidInputField.should('be.visible')
      .and('have.attr', 'id', 'cc-exp');
  });

  it('user should not be able to proceed if card expiration date is missing', () => {
    userData.expDate = null;

    shopPage.addProductsToCart(previewTargetProducts);
    shopPage.open(relativeUrls.checkout);
    checkoutPage.openPayments();
    paymentsPage.enterPersonalData(userData);
    paymentsPage.enterCardData(userData);
    paymentsPage.invalidInputField.should('be.visible')
      .and('have.attr', 'id', 'cc-exp');
  });

  it('user should not be able to proceed if cvv is invalid', () => {
    userData.cvv = '1';

    shopPage.addProductsToCart(previewTargetProducts);
    shopPage.open(relativeUrls.checkout);
    checkoutPage.openPayments();
    paymentsPage.enterPersonalData(userData);
    paymentsPage.enterCardData(userData);
    paymentsPage.invalidInputField.should('be.visible')
      .and('have.attr', 'id', 'cc-csc');
  });

  it('user should not be able to proceed if cvv is missing', () => {
    userData.cvv = null;

    shopPage.addProductsToCart(previewTargetProducts);
    shopPage.open(relativeUrls.checkout);
    checkoutPage.openPayments();
    paymentsPage.enterPersonalData(userData);
    paymentsPage.enterCardData(userData);
    paymentsPage.invalidInputField.should('be.visible')
      .and('have.attr', 'id', 'cc-csc');
  });

  it('user should not be able to pay for purchases if billing name is missing', () => {
    userData.billingAddress = new BillingAddress();
    userData.billingAddress.name = null;

    shopPage.addProductsToCart(previewTargetProducts);
    shopPage.open(relativeUrls.checkout);
    checkoutPage.openPayments();
    paymentsPage.enterPersonalData(userData);
    paymentsPage.invalidInputField.should('be.visible')
      .and('have.attr', 'id', 'billing-name');
  });

  it('user should not be able to pay for purchases if billing street address is missing', () => {
    userData.billingAddress = new BillingAddress();
    userData.billingAddress.line1 = null;

    shopPage.addProductsToCart(previewTargetProducts);
    shopPage.open(relativeUrls.checkout);
    checkoutPage.openPayments();
    paymentsPage.enterPersonalData(userData);
    paymentsPage.invalidInputField.should('be.visible')
      .and('have.attr', 'id', 'billing-street');
  });

  it('user should not be able to pay for purchases if billing postal code is missing', () => {
    userData.billingAddress = new BillingAddress();
    userData.billingAddress.zip = null;

    shopPage.addProductsToCart(previewTargetProducts);
    shopPage.open(relativeUrls.checkout);
    checkoutPage.openPayments();
    paymentsPage.enterPersonalData(userData);
    paymentsPage.invalidInputField.should('be.visible')
      .and('have.attr', 'id', 'billing-zip');
  });

  it('user should not be able to pay for purchases if billing city is missing', () => {
    userData.billingAddress = new BillingAddress();
    userData.billingAddress.city = null;

    shopPage.addProductsToCart(previewTargetProducts);
    shopPage.open(relativeUrls.checkout);
    checkoutPage.openPayments();
    paymentsPage.enterPersonalData(userData);
    paymentsPage.invalidInputField.should('be.visible')
      .and('have.attr', 'id', 'billing-city');
  });
});
