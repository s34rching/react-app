import BasePage from './basepage.page';

class HomePage extends BasePage {
  get directoryMenu() {
    return cy.get('#directory-menu');
  }

  goToOverviewSection(sectionTitle) {
    this.directoryMenu.contains(new RegExp(`^${sectionTitle}$`)).click();
  }
}

export default HomePage;
