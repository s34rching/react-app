import { $, $$, browser } from '@wdio/globals';
import BasePage from './base.page';

class SearchResultsPage extends BasePage {
  get emptyResultsContainer() {
    return $('p=Nothing found...');
  }

  getFoundProductById(id) {
    return $(`[data-test=item-container-${id}]`);
  }

  async getAllFoundProducts(count) {
    const productCardSelector = '[data-test^=item-container-]';

    await browser.waitUntil(async () => await $$(productCardSelector).length === count);
    return $$(productCardSelector);
  }
}

export default SearchResultsPage;
