import Chance from 'chance';
import HomePage from '../pages/home.page';
import SearchResultsPage from '../pages/search-results.page';
import { relativeUrls } from '../../common/relative-urls';
import { getRandomProduct } from '../../common/data-providers';
import { filterProductsByNameSubstring, getProductNameSubstring } from '../../common/data-handlers';

const chance = new Chance();
const homePage = new HomePage();
const searchResultsPage = new SearchResultsPage();
const product = getRandomProduct();

describe('Search', () => {
  beforeEach(async () => {
    await homePage.open(relativeUrls.home);
  });

  it('user should be able to search for products by exact product name', async () => {
    await homePage.searchForProduct(product.name);
    await expect(searchResultsPage.getFoundProductById(product.id)).toBeDisplayed();
  });

  it('user should be able to search for products by product name substring', async () => {
    const query = getProductNameSubstring(product.name);
    const expectedProducts = filterProductsByNameSubstring(query);

    await homePage.searchForProduct(query);
    await expect(searchResultsPage.getAllFoundProducts(expectedProducts.length))
      .toBeElementsArrayOfSize(expectedProducts.length);
  });

  it('empty SERP should be displayed when user submits invalid search query', async () => {
    const query = chance.sentence({ words: 3 });

    await homePage.searchForProduct(query);
    await expect(searchResultsPage.emptyResultsContainer).toBeDisplayed();
  });

  it('user should not be able to submit empty query', async () => {
    const query = ' ';

    await homePage.searchForProduct(query);
    await expect(homePage.getUrl()).not.toContain(relativeUrls.search(query));
  });
});
