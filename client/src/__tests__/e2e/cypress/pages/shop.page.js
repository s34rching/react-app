import _ from 'lodash';
import BasePage from './basepage.page';

class ShopPage extends BasePage {
  get shopContainer() {
    return cy.get('.shop-page');
  }

  get productCards() {
    return cy.get('[data-test^="item-container-"]');
  }

  get previewSectionTitles() {
    return this.shopContainer
      .find('a[href]')
      .then((titles) => (
        Cypress.$.makeArray(titles)
          .map((el) => el.innerText)
      ));
  }

  getProductCardById(productId) {
    return cy.get(`[data-test="item-container-${productId}"]`);
  }

  getAddToCartButton(productId) {
    return this.getProductCardById(productId)
      .find('button')
      .contains('Add to Cart');
  }

  addProductsToCart(products) {
    products.forEach((product) => {
      const { id, count } = product;

      _.times(count, () => {
        this.getAddToCartButton(id)
          .scrollIntoView()
          .invoke('show')
          .click();
      });
    });
  }
}

export default ShopPage;
