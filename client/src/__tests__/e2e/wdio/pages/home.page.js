import { $ } from '@wdio/globals';
import BasePage from './base.page';

class HomePage extends BasePage {
  get directoryMenu() {
    return $('#directory-menu');
  }

  async goToOverviewSection(sectionTitle) {
    await this.directoryMenu.$(`span=${sectionTitle}`).click();
  }
}

export default HomePage;
