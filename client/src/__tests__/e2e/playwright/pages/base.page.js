import { expect } from '@playwright/test';

class BasePage {
  constructor(page) {
    this.page = page;
    this.signOutButton = page.locator('[data-test="sign-out-button"]');
    this.signInButton = page.locator('[href="/signing"]', { hasText: 'SIGN IN' });
    this.footerSignUpLink = page.locator('[data-test="footer"] a[href="/signing"]');
    this.searchField = page.locator('input[name="search"]');
    this.cartItemsCounter = page.locator('[data-test="cart-items-counter"]');
    this.cartEmptyMessage = page.locator('span', { hasText: 'Your cart is empty' });
    this.goToCheckoutButton = page.locator('button', { hasText: 'GO TO CHECKOUT' });
    this.header = page.locator('[data-test="header"]');
    this.footer = page.locator('[data-test="footer"]');
    this.feedbackFormTitle = page.locator('h2', { hasText: 'CUSTOMER FEEDBACK FORM' });
    this.logo = page.locator('#site-logo');
  }

  async open(relativePath) {
    await this.page.goto(`localhost:3000${relativePath}`);
  }

  async searchForProduct(productName) {
    await this.searchField.type(productName);
    await this.searchField.press('Enter');
  }

  async openCart() {
    await this.cartItemsCounter.click();
  }

  async goToCheckout() {
    await this.openCart();
    await this.goToCheckoutButton.click();
  }

  async openFeedbackForm() {
    await this.footer
      .locator('h4', { hasText: 'SEND US FEEDBACK' })
      .click();
  }

  async goToShop() {
    await this.header
      .locator('a', { hasText: 'SHOP' })
      .click();
  }

  async goToAboutUs() {
    await this.header
      .locator('a', { hasText: 'ABOUT US' })
      .click();
  }

  async goToSigning() {
    await this.signInButton.click();
  }

  async getFooterLinkByUrl(url) {
    await this.footer.locator(`a[href="${url}"]`);
  }

  async assertCartItems(addedProducts) {
    await Promise.all(
      addedProducts.map(async (product) => {
        const { name, count, price } = product;

        await expect(this.page.locator(`[data-test="item-name-${product.id}"]`)).toBeVisible();
        await expect(this.page.locator(`[data-test="item-name-${product.id}"]`)).toContainText(name);
        await expect(this.page.locator(`[data-test="item-total-${product.id}"]`)).toBeVisible();
        await expect(this.page.locator(`[data-test="item-total-${product.id}"]`)).toContainText(`${count}x${price}`);
      }),
    );
  }

  async assertFooterLink(link) {
    await expect(this.footer.locator(`a[href="${link.path}"]`)).toBeVisible();
    await expect(this.footer.locator(`a[href="${link.path}"]`)).toHaveText(link.text);
  }

  async assertFooterIconLink(link) {
    await expect(this.footer.locator(`a[href="${link.url}"]`)).toBeVisible();

    const target = await this.footer.locator(`a[href="${link.url}"]`).evaluate((e) => e.target);
    await expect(target).toBe('_blank');

    const svgIcon = await this.footer.locator(`a[href="${link.url}"]`).locator('svg');
    const iconWidth = await svgIcon.evaluate((e) => window.getComputedStyle(e).getPropertyValue('width'));
    const iconHeight = await svgIcon.evaluate((e) => window.getComputedStyle(e).getPropertyValue('height'));

    await expect(iconWidth).toBe(`${link.width}px`);
    await expect(iconHeight).toBe(`${link.height}px`);
  }

  async assertExternalFooterLink(link) {
    await expect(this.footer.locator(`a[href="${link.url}"]`)).toBeVisible();
    await expect(this.footer.locator(`a[href="${link.url}"]`)).toHaveText(link.text);
    const target = await this.footer.locator(`a[href="${link.url}"]`).evaluate((e) => e.target);
    await expect(target).toBe('_blank');
  }
}

export default BasePage;
