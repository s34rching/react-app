import { test, expect } from '@playwright/test';
import SigningPage from '../pages/signing.page';
import errors from '../../common/errors';
import { relativeUrls } from '../../common/relative-urls';
import credentials from '../../common/credentials';
import User from '../../common/entities/user';
import { getRandomPassword } from '../../common/data-providers';

test.describe('Sign In', () => {
  let userData;
  const { login, password } = credentials.valid;

  test('user should be able to sign in with valid data', async ({ page }) => {
    userData = new User({ email: login, password });
    const signInPage = new SigningPage(page);

    await signInPage.open(relativeUrls.signing);
    await signInPage.signIn(userData);
    await expect(signInPage.signOutButton).toBeVisible();
  });

  test('user should NOT be able to sign in with wrong password', async ({ page }) => {
    userData = new User({ email: login, password: getRandomPassword() });
    const signInPage = new SigningPage(page);

    await signInPage.open(relativeUrls.signing);
    await signInPage.signIn(userData);
    await expect(signInPage.errorMessage).toBeVisible();
    await expect(signInPage.errorMessage).toHaveText(errors.invalidPassword);
  });

  test('user should NOT be able to sign in with non-registered email', async ({ page }) => {
    userData = new User();
    const signInPage = new SigningPage(page);

    await signInPage.open(relativeUrls.signing);
    await signInPage.signIn(userData);
    await expect(signInPage.errorMessage).toBeVisible();
    await expect(signInPage.errorMessage).toHaveText(errors.nonRegisteredUser);
  });

  test('user should NOT be able to sign in with empty email', async ({ page }) => {
    userData = new User();
    userData.email = null;
    const signInPage = new SigningPage(page);

    await signInPage.open(relativeUrls.signing);
    await signInPage.signIn(userData);
    await expect(signInPage.failedSignInEmailInput).toBeVisible();
  });

  test('user should NOT be able to sign in with empty password', async ({ page }) => {
    userData = new User();
    userData.password = null;
    const signInPage = new SigningPage(page);

    await signInPage.open(relativeUrls.signing);
    await signInPage.signIn(userData);
    await expect(signInPage.failedSignInPasswordInput).toBeVisible();
  });

  test('"Sign Up" link in footer should not exist if user is signed in', async ({ page }) => {
    userData = new User({ email: login, password });
    const signInPage = new SigningPage(page);

    await signInPage.open(relativeUrls.signing);
    await signInPage.signIn(userData);
    await expect(signInPage.signOutButton).toBeVisible();
    await expect(signInPage.footerSignUpLink).not.toBeVisible();
  });

  test('user should be able to sign out', async ({ page }) => {
    userData = new User({ email: login, password });
    const signInPage = new SigningPage(page);

    await signInPage.open(relativeUrls.signing);
    await signInPage.signIn(userData);
    await expect(signInPage.signOutButton).toBeVisible();

    await signInPage.signOutButton.click();
    await expect(signInPage.signInButton).toBeVisible();
  });
});
