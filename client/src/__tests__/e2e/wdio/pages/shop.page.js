import { $, $$, browser } from '@wdio/globals';
import { times } from 'lodash';
import BasePage from './base.page';
import { getTargetProductsCount } from '../../common/data-handlers';
import { waitForAnimation } from '../../../utils/page-interactors';

class ShopPage extends BasePage {
  get shopContainer() {
    return $('.shop-page');
  }

  get productCards() {
    return $$('[data-test^="item-container-"]');
  }

  get sectionTitles() {
    return this.shopContainer.$$('a[href]');
  }

  async addProductToCart(product) {
    const { id, count } = product;
    const selector = `[data-test="item-container-${id}"]`;
    const buttonSelector = 'button=Add to Cart';

    await waitForAnimation(selector);

    const y = await $(selector).getLocation('y');
    const scrolledDistance = await browser.execute(() => window.scrollY);
    const distanceToScroll = (scrolledDistance) ? (y - scrolledDistance) : y;

    await browser.scroll(0, Number.parseInt(distanceToScroll, 10));

    times(count, async () => {
      await $(selector).moveTo();
      await $(selector).$(buttonSelector).waitForClickable();
      await $(selector).$(buttonSelector).click();
    });
  }

  async addProductsToCart(products, cartProductsCount) {
    const count = cartProductsCount || getTargetProductsCount(products);

    await products.reduce(async (previous, next) => {
      await previous;
      await this.addProductToCart(next);
    }, Promise.resolve());

    await browser.waitUntil(async () => {
      const itemsCount = await this.cartItemsCounter.getText();
      return itemsCount === count.toString();
    });
  }
}

export default ShopPage;
