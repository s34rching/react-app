import BasePage from './basepage.page';

class SigningPage extends BasePage {
  get signInEmail() {
    return cy.get("[data-test='sign-in-email']");
  }

  get signInPassword() {
    return cy.get("[data-test='sign-in-password']");
  }

  get submitSignInButton() {
    return cy.get("[data-test='sign-in-submit']");
  }

  get submitSignInWithGoogleButton() {
    return cy.get("[data-test='google-sign-in']");
  }

  get signUpName() {
    return cy.get("[data-test='sign-up-name']");
  }

  get signUpEmail() {
    return cy.get("[data-test='sign-up-email']");
  }

  get signUpPassword() {
    return cy.get("[data-test='sign-up-password']");
  }

  get signUpConfirmPassword() {
    return cy.get("[data-test='sign-up-confirm-password']");
  }

  get submitSignUpButton() {
    return cy.get("[data-test='sign-up-submit']");
  }

  get errorMessage() {
    return cy.get("[data-test='signing-error']");
  }

  signIn({ email, password }) {
    if (email) {
      this.signInEmail.type(email);
    }
    if (password) {
      this.signInPassword.type(password);
    }
    this.submitSignInButton.click();
  }

  signInWithGoogle() {
    this.submitSignInWithGoogleButton.click();
  }

  signUp({ name, email, password, confirmPassword = password }) {
    if (name) {
      this.signUpName.type(name);
    }
    if (email) {
      this.signUpEmail.type(email);
    }
    if (password) {
      this.signUpPassword.type(password);
    }
    if (confirmPassword) {
      this.signUpConfirmPassword.type(confirmPassword);
    }

    this.submitSignUpButton.click();
  }
}

export default SigningPage;
