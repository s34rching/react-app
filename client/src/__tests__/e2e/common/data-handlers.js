import _ from 'lodash';
import constants from './constants';
import { getAllProducts, getRandomCount } from './data-providers';

const { MAX_ITEMS_COUNT } = constants;

export const capitalize = (word) => _.capitalize(word);
export const toUpper = (word) => _.toUpper(word);
export const filterProductsByNameSubstring = (substring) => {
  const regExp = new RegExp(`${substring}`, 'i');

  return _.filter(getAllProducts(), (p) => regExp.test(p.name));
};

export const getProductNameSubstring = (name) => _.sample(name.split(' '));

export const getCartTotal = (targetProducts) => (
  _.reduce(targetProducts, (current, next) => (
    current + next.count * next.price
  ), 0)
);

export const getUpdatedProducts = (operation, products, product, count) => {
  const initialProducts = [ ...products ];

  if (operation === 'remove') {
    return _.reject(initialProducts, { id: product.id });
  }
  if (operation === 'increase') {
    return _.map(initialProducts, (initialProduct) => {
      if (initialProduct.id === product.id) {
        return { ...initialProduct, count: initialProduct.count + count };
      }
      return { ...initialProduct };
    });
  }
  if (operation === 'reduce') {
    return _.map(initialProducts, (initialProduct) => {
      if (initialProduct.id === product.id) {
        return { ...initialProduct, count: initialProduct.count - count };
      }
      return { ...initialProduct };
    });
  }
};

export const getProductsMap = (products, productsCount = 1, minItemsCount = 2) => {
  const productsToMap = _.sampleSize(products, productsCount);

  return _.map(productsToMap, (product) => (
    {
      ...product,
      count: getRandomCount(minItemsCount, MAX_ITEMS_COUNT),
    }
  ));
};

export const getTargetProductsCount = (products) => (_.reduce(products, (current, next) => (
  current + next.count
), 0)).toString(10);
